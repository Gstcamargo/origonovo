<form class="loginform" method="post" action="">
  <div class="bg-cinza espacamento-bgcinza">
    <div class="container text-center">
      <span class="texto3 font-gotham-black">FAÇA SEU LOGIN</span>
        <div class="text-center espacamento20"><img src="<?= base_url() ?>img/divisao.jpg" /></div>
    	  <div class="box-login">
          <div class="coluna-cadastro">
           	<div class="borda-cadastre text-left">
             	<div class="titulo-input-home">E-mail</div>
             	<input type="text" id="email" name="email" id="recipient-name" class="input input-transparente-login"/>
            </div>
            <div class="borda-cadastre text-left espacamento-top">
             	<div class="titulo-input-home">Senha</div>
              <input type="password" id="recipient-name" id="senha" name="senha" class="input-transparente-login"/>
            </div>
            <div class="espacamento-top box100-floatleft">
           		<div class="text-left input-permanecer-logado">
               	<input name="" type="checkbox" value="" /> Permanecer logado
              </div>
              <div class="text-left floatleft-width">
                <button type="submit" class="botao" style="width:100%">ENTRAR</button>
         			</div>
            </div>
            <div class="espacamento-top box100-floatleft">
              <div>Ainda não tem Cadastro? Clique <a href="<?= base_url("cadastro") ?>"> aqui</a></div>
            </div>
          </div>
        </div>
    </div>
  </div>
</form>