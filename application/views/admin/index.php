<title>Administração</title>
<!-- Compiled and minified CSS -->
<link rel="stylesheet" href="<?= base_url()?>css/materialize.min.css">
<!--Import Google Icon Font-->
<link href="http://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />

<!-- Compiled and minified JavaScript -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
<nav class="white">
	<div class="nav-wrapper container valign-wrapper">
		<a href="#" class="brand-logo black-text"><img class="valign"
			alt="Logo origo" src="<?= base_url("img/logo-preto.png")?>"></a>
		<ul id="nav-mobile" class="right hide-on-med-and-down">

		</ul>
	</div>
</nav>
<div class="row">
	<div class="col s12 m4">
		<div class="card">
			<div class="card-content">
				<h5>Cadastro de marca</h5>
				<p>Esse é o cadastro virtual, aqui é cadastrado o site, cada site
					tem sua tabela da medidas, alguns raramente varia por marcas</p>
				<form method="post" action="" id="form1">
					<div class="container">
						<div id="step1">
							<input placeholder="www.link.com.br" name="host" type="text"
								class="validate"> <input placeholder="Marca" name="marca"
								type="text"> <input placeholder="P,M,G..." name="medida"
								type="text" class="validate"> <a
								class="waves-effect waves-light  right"><i
								class="material-icons right" id="next">arrow_forward</i>Next</a>
				
			</div>
			<div id="step2" hidden>

					<div class="col-lg-12" id="tamanho">
<?php

$arr = array (
		'ombro' => 'Ombro',
		'tamanho_braco' => 'Tamanho do braço',
		'busto' => 'Busto',
		'cintura' => 'Cintura',
		'quadril' => 'Quadril',
		'tamanho_perna' => 'Tamanho da perna',
		'comprimento' => 'Comprimento',
		'punho' => 'Punho',
		'gancho' => 'Gancho',
		'coxa' => 'Coxa',
		'panturrilha' => 'Panturrilha',
		'abertura_barra' => 'Abertura da Barra',
		'largura_barra' => 'Largura da Barra' 
);

foreach ( $arr as $key => $value ) :
	echo "<div class='row' id='select$key'>
                                <div class='col-lg-4'>
                                    <label> $value (cm) </label>
                                </div>
                                <div class='form-group col-lg-6'>              
                                    <input type='text' id='my$key' name='$key' onchange='myFunction(this.id, this.name)' min='0' max='148'>
                                </div>
                            </div>";
endforeach
;
?>
						</div>

					<a class="waves-effect waves-light  right " id="save"> Salvar </a>
			
			</div>

		</div>
		</form>

	</div>
</div>
</div>

<div class="col s12 m8">
	<div class="card">
		<div class="card-content">
			<h5>Informações sobre usuarios</h5>
		</div>
	</div>
</div>
</div>
<script src="<?= base_url("js/jquery.min.js") ?>"></script>
<script type="text/javascript">
$(document).ready(function(){
	
});
$("#next").click(function(){
	$("#step1").slideToggle(function() {
		$("#step2").show();
	});
});
$("#save").click(function(){
	$("#step1").show();
		$("#form1").submit();
	
});
</script>
