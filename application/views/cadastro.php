
<div class="bg-cinza espacamento-bgcinza">
	<div class="container text-center">
		<form class="loginform col-lg-4 col-lg-offset-4" method="post"
			action="" name="oi">
			<span class="texto3 font-gotham-black">CADASTRE-SE</span>
			<div class="text-center espacamento20">
				<img src="img/divisao.jpg" />
			</div>
			<div class="box-cadastre">
				<div class="coluna-cadastro">
					<div class="borda-cadastre text-left">
						<div class="titulo-input-home">Nome Completo</div>
						<input type="hidden" name="cadastro" value="true" /> <input
							type="text" class="input input-transparente-cadastro" id="name"
							name="name" required>
					</div>
					<div class="espacamento-top box100-floatleft">
						<div class="borda-cadastre text-left box-senha-datanasc">
							<div class="titulo-input-home">Data de Nascimento</div>
							<input type="text" class="input input-transparente-cadastro"
								id="birth" name="birth" required>
						</div>
						<div class="borda-cadastre text-left floatleft-width">
							<div class="titulo-input-home">Cidade</div>
							<input type="text" class="input input-transparente-cadastro"
								id="city" name="city" required>
						</div>
					</div>
					<div
						class="borda-cadastre text-left espacamento-top box100-floatleft">
						<div class="titulo-input-home">E-mail</div>
						<input type="email" class="input input-transparente-cadastro"
							id="email" name="email" required>
					</div>
					<div class="espacamento-top box100-floatleft">
						<div class="borda-cadastre text-left box-senha-datanasc">
							<div class="titulo-input-home">Senha</div>
							<input type="password" class="input input-transparente-cadastro"
								id="senha" name="password" required>
						</div>
						<div class="borda-cadastre text-left floatleft-width">
							<div class="titulo-input-home">Username</div>
							<input type="text" class="input input-transparente-cadastro"
								id="username" name="username" required>
						</div>
					</div>
					<div
						class="borda-cadastre text-left espacamento-top box100-floatleft">
						<div class="titulo-input-home">Sexo</div>
						<select class="input-transparente-cadastro" name="gender">
<?php
$arr = array (
		"wc" => "Who cares?",
		"m" => "Masculino",
		"f" => "Feminino" 
);

foreach ( $arr as $key => $value ) :
	echo "<option id='$key' name='gender' value='$key'>$value</option>";
endforeach
;
?>
                                    </select>
					</div>
					<div class="espacamento-top box100-floatleft">
						<button type="submit" class="botao" style="width: 100%">CADASTRAR</button>
					</div>
					<div class="espacamento-top box100-floatleft">
						<div>
							Já tem login? Clique <a href="<?= base_url("login") ?>">aqui</a>
						</div>
					</div>

				</div>
		
		</form>
	</div>
</div>
</div>
<script src="<?=base_url("js/jquery.js") ?>"></script>

<script src="<?=base_url("js/datemask.js") ?>"></script>
<script>

jQuery(function($){
    $("#birth").mask("99/99/9999",{placeholder:"dd/mm/aaaa"});
	});
        </script>