
<div class="rodape">
	<div class="container">
		<span class="font-gotham-medium espacamento-font">INSCREVA-SE PARA
			RECEBER NOSSO CONTEÚDO</span><br />
		<br />

		<div class="row">
			<div class="box-input-news">
				<div class="coluna2">
					<div class="input-news">
						<form action="//Origo.us14.list-manage.com/subscribe/post?u=e1b66f2134081f048038d76e6&amp;id=6492279c36" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" novalidate>
							<input type="email" value="" name="EMAIL" class="email input-transparente estilo-newsletter" id="mce-EMAIL" placeholder="Email" required>
							<input type="image" name="submit" class="ir-news" src="<?= base_url() ?>img/ir.jpg" />
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="box-siganos">
			<div class="coluna">
				<span class="siganos">SIGA A ORIGO:</span>
				<div class="redes-sociais">
					<a href="https://www.facebook.com/origocc/" target="_blank"><img
						src="<?= base_url() ?>img/facebook.png" /></a>
				</div>
				<!--<div class="redes-sociais"><img src="img/twitter.png" /></div>-->
				<div class="redes-sociais">
					<a href="https://www.instagram.com/ORIGO.CC/" target="_blank"><img
						src="<?= base_url() ?>img/instagram.png" /></a>
				</div>
			</div>
		</div>

		<div class="font-gotham espacamento-copyright">
			© Copyright 2016. Todos os direitos reservados. Nossa <a
				href="politica/nossapoliticadeprivacidade-Origo.pdf"
				target="_blank">Política de Privacidade</a>.
		</div>
		<div class="row">
			<div class="box-input-news"><button class="btn-sm btn-info">Costureiras</button>
			</div>
		</div>

	</div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>