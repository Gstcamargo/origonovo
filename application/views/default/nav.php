

<div class="row visible-lg visible-md">
	<div class="container">
		<div class="col-md-4 col-lg-4 text-left">
			<a href="<?= base_url() ?>"><img src="<?= base_url() ?>img/logo-preto.png" class="logo"></a>
		</div>
       <?php if (isset($id)): ?>
      <div class="col-md-4 col-xs-12 col-sm-4 text-right estrutura-menu">
        <span class="font-gotham-medium altura-linha">
        </span>
      </div>
      <div class="col-md-4 col-xs-12 col-sm-4 text-right estrutura-menu">
        <div class="btn-group">
            <a style="color:black" class="dropdown-toggle estilo-dropdown" data-toggle="dropdown"><strong>Olá <?= $nome ?></strong></span>&nbsp;<span class="glyphicon glyphicon-menu-down"></a>
            <ul class="dropdown-menu" role="menu">
                <li>
                <a style="color:black" href="<?= base_url('provador/')?>">Provador Virtual</a>
                </li>
                
                <li><a style="color:black" href="<?= base_url('login/logout')?>">Sair</a></li>
            </ul>
        </div>
      </div>
<?php else: ?>
		<div class="col-md-5 col-lg-5 text-right mtop25">
			<span class="font-gotham-medium altura-linha"> <a href="<?= base_url() ?>login">Entrar</a>
				<span class="texto4">ou</span> <a href="<?= base_url() ?>cadastro">Cadastre-se</a>
			</span>
		</div>
 
		<div class="col-md-3 col-lg-3 text-right mtop25">
			<a href="<?= base_url() ?>cadastro" class="button-create font-gotham-medium"><strong>CRIAR
					SEU BODYID</strong></a>
		</div>
		 <?php endif;?>
	</div>
</div>
