<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<link rel="icon" href="favicon.png" type="image/png" sizes="16x16">
<link rel="stylesheet" href="<?= base_url() ?>css/bootstrap-theme.css">
<link rel="stylesheet" href="<?= base_url() ?>css/bootstrap.css">
<link rel="stylesheet" href="<?= base_url() ?>css/custom.css">
<link rel="stylesheet" href="<?= base_url() ?>css/estilo.css">

</head>