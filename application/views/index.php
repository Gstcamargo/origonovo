
<!--header até aqui-->


<div id="carousel-origo" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<li data-target="#carousel-origo" data-slide-to="0" class="active"></li>
		<li data-target="#carousel-origo" data-slide-to="1"></li>
		<li data-target="#carousel-origo" data-slide-to="2"></li>
		<li data-target="#carousel-origo" data-slide-to="3"></li>
		<li data-target="#carousel-origo" data-slide-to="4"></li>
		<li data-target="#carousel-origo" data-slide-to="5"></li>
		<li data-target="#carousel-origo" data-slide-to="6"></li>
	</ol>

	<!-- Wrapper for slides -->
	<div class="carousel-inner" role="listbox">
		<div class="item active">
			<img src="img/01banner.jpg" width="100%" height="300px">
			<div class="carousel-caption">
				<div class="title">
					<span class="titulo-header title">RESPEITE SUAS MEDIDAS</span><br />
					<span class="subtitulo-header">Nós sabemos que cada corpo é único.</span>
				</div>
			</div>
		</div>
		<div class="item">
			<img src="img/02banner.jpg" width="100%" height="300px">
			<div class="carousel-caption">
				<div class="title">
					<span class="titulo-header">O MATCH PERFEITO </span><br /> <span
						class="subtitulo-header">entre as medidas do seu corpo e as
						medidas da sua roupa.</span>
				</div>
			</div>
		</div>
		<div class="item">
			<img src="img/03banner.jpg" width="100%" height="300px">
			<div class="carousel-caption">
				<div class="title">
					<span class="titulo-header">VAMOS NOS REAPROPRIAR DA MODA!</span><br />
					<span class="subtitulo-header"></span>
				</div>
			</div>
		</div>
		<div class="item">
			<img src="img/04banner.jpg" width="100%" height="300px">
			<div class="carousel-caption">
				<div class="title">
					<span class="titulo-header">VOCÊ NÃO PRECISA MAIS SE PREOCUPAR COM
						P,M,G, 36, 42, 52.</span><br /> <span class="subtitulo-header">Nós
						sabemos que cada corpo é único e tem suas medidas. E respeitamos
						isso.</span>
				</div>
			</div>
		</div>
		<div class="item">
			<img src="img/05banner.jpg" width="100%" height="300px">
			<div class="carousel-caption">
				<div class="title">
					<span class="titulo-header">EMPODERE-SE DE SEU PRÓPRIO CORPO</span><br />
					<span class="subtitulo-header">conheça suas medidas. Faça seu body
						id</span>
				</div>
			</div>
		</div>
		<div class="item">
			<img src="img/06banner.jpg" width="100%" height="300px">
			<div class="carousel-caption">
				<div class="title">
					<span class="titulo-header">RESPEITE SUAS MEDIDAS</span><br /> <span
						class="subtitulo-header">A gente sabe que seu corpo é único.</span>
				</div>
			</div>
		</div>
		<div class="item">
			<img src="img/07banner.jpg" width="100%" height="300px">
			<div class="carousel-caption">
				<div class="title">
					<span class="titulo-header">RESPEITE SUAS MEDIDAS</span><br /> <span
						class="subtitulo-header">A gente sabe que seu corpo é único.</span>
				</div>
			</div>
		</div>
	</div>
</div>

<!--header-->



<div class="row cta">

	<div class="titulos-gotham">UTILIZE NOSSO PROVADOR VIRTUAL</div>
	<br />
	<br />

	<div class="container">
		<div class="col-md-4 bg-white">
			<a class="botao2">Experimente uma roupa</a>
		</div>
		<div class="col-md-1 ou">ou</div>
		<div class="col-md-7 borda-provar">


              <div class="row text-left">
				<div class="col-md-5 divisao-username">
					<div class="row titulo-input-home">Link do produto</div>
					<div class="row">
						<input type="text" name="url"
							placeholder="Insira o link do produto" class="input-transparente" />
					</div>
				</div>
				<div class="col-md-5">
					<div class="row titulo-input-home">Username</div>
					<div class="row">
						<input type="text" placeholder="Insira seu Username"
							class="input-transparente" />
					</div>
				</div>

				<div class="col-md-2 bg-white text-center">
					<a href="#" class="botao2">PROVAR</a>
				</div>
                </div>

		</div>

	</div>


	<div class="row espacamento30 texto2">
		Ainda não cadastrou seu BodyID? <strong class="font-gotham-black"><a
			href="cadastro.php" class="btn-cadastro-bodyid">Cadastre-se</a></strong>
	</div>


</div>



<div class="bg-cinza">

	<div class="container text-center">
		<p class="texto3">
			O <span class="font-gotham-black">match </span>perfeito entre as
			medidas do<br />seu corpo e as medidas da roupa.
		</p>
	</div>

	<div class="text-center espacamento20">
		<img src="img/divisao.jpg" />
	</div>

	<div class="container text-center">
		<p class="texto4">
			Com a ORIGO você não precisa mais se preocupar com P, M, G, 36, 42,
			52. <br /> Nós sabemos que cada corpo é único e tem suas medidas. E
			respeitamos isso.
		</p>
	</div>

</div>


<!--como funciona-->
<div class="como-funciona text-center">
	<span class="font-gotham-black"><h3>COMO FUNCIONA</h3></span> <span
		class="font-gotham"><h4>o provador virtual?</h4></span>


	<div class="container espacamento30">

		<div class="row">


			<div class="col-md-3">
				<img src="img/website.jpg" /><br />
				<br /> <span class="titulo-como-funciona">escolha suas peças
					favoritas</span><br />
				<br /> <span class="font-gotham-book"> Nos sites das suas marcas
					preferidas e escolha as peças que você quer experimentar.</span>
			</div>
			<div class="col-md-1 visible-lg visible-md">
				<span class="glyphicon glyphicon-menu-right glyphicon-size"></span>
			</div>

			<div class="visible-xs visible-sm">
				<br />
			</div>

			<div class="col-md-3 espacamento-esquerda">
				<img src="img/clothes.jpg" /><br />
				<br /> <span class="titulo-como-funciona">prove suas <br />roupas
				</span><br />
				<br /> <span class="font-gotham-book"> Venha pro site ORIGO, copie e
					cole o link da peça escolhida no provador virtual.</span>
			</div>
			<div class="col-md-1  visible-lg visible-md">
				<span class="glyphicon glyphicon-menu-right glyphicon-size"></span>
			</div>

			<div class="visible-xs visible-sm">
				<br />
			</div>

			<div class="col-md-3 espacamento-esquerda">
				<img src="img/choose.jpg" /><br />
				<br /> <span class="titulo-como-funciona">Veja o tamanho sugerido</span><br />
				<br /> <span class="font-gotham-book">Após cadastrar suas medidas no
					bodyID, veja o tamanho sugerido pra você.</span>
			</div>


		</div>

		<div class="text-center font-gotham-book espacamento30">
			As informações acima te ajudaram?
			<!--mailto:cintia@origo.cc?Subject=Sim%20me%20ajudou!-->
			<!--mailto:cintia@origo.cc?Subject=Nao%20me%20ajudou!-->
			<span class="simnao"><a href="contatosim.php">Sim</a></span> ou <span
				class="simnao"><a href="contatonao.php">Não</a></span>.
		</div>

	</div>
</div>


