<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">

<!-- Compiled and minified JavaScript -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
<div class="container">
	<h1 class="center">Origo | Provador virtual</h1>
	<div class="app" style="height: 500px">
		<div id="load_panel">
			<div class="progress">
				<div class="indeterminate"></div>
			</div>
			<p class="center" id="load">Tirando medidas</p>
			<div class="row">
				<div class="col s12 m6 offset-m3">
					<div class="card blue darken-1">
						<div class="card-content white-text">
							<span class="card-title center">Curiosidade</span>
							<p>Botões</p>
							<p>O botão existe desde 3000 anos antes de Cristo. Existem
								diversos tipos de botões. Os primeiros eram feitos de osso e
								metal.</p>
						</div>

					</div>
				</div>
			</div>


		</div>
		<div id="panel1" hidden>

			<div class="row">
				<div class="col s6 m6">
					<!-- <div class="card">

						<div class="container">
							<div class="card-title">Suas medidas</div>
							
						</div>
					</div> -->
					<div class="panel panel-primary">
						<div class="panel-heading">Sobre você</div>
						<div class="panel-body" style="float: none;">
							<div class="row">
								<div class="col-sm-6 col-md-6">

									<div class="thumbnail">
										<img src="<?= base_url("img/iconpeca.png") ?>" alt="...">
										<div class="caption">
											<p class="center">
												<b>Minhas peças</b>
											
											
											<ul class="center">
												<li><a href="#">Ver</a></li>
												<li><a href="#">Provar</a></li>
											</ul>

											<p>
										
										</div>
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<ul class="collection" id="minhas_medidas" hidden>
										<li class="collection-item">Altura<span
											class="secondary-content"><?= $medida->height?> cm</span></li>
										<li class="collection-item">Peso<span
											class="secondary-content"><?= $medida->weight?> cm</span></li>
										<li class="collection-item">Ombro<span
											class="secondary-content"><?= $medida->shoulder?> cm</span></li>
										<li class="collection-item">Busto<span
											class="secondary-content"><?= $medida->bust?> cm</span></li>
										<li class="collection-item">Cintura<span
											class="secondary-content"><?= $medida->waist?> cm</span></li>
										<li class="collection-item">Bra&ccedil;o<span
											class="secondary-content"><?= $medida->arm?> cm</span></li>
										<li class="collection-item">Quadril<span
											class="secondary-content"><?= $medida->hip?> cm</span></li>
										<li class="collection-item">Perna<span
											class="secondary-content"><?= $medida->legs?> cm</span></li>
										<li class="collection-item"><a href="#" id="backpanelmedidas">Voltar</a></li>
									</ul>
									<div class="thumbnail" id="panel_medidas">
										<img src="<?= base_url("img/iconmeasure.png") ?>" alt="...">
										<div class="caption">
											<p class="center">
												<b>Editar Medidas</b>
											
											
											<ul class="center">
												<li><a href="#" id="ver_medidas">Ver</a></li>
												<li><a href="<?= base_url("provador/editarbasicas")?>">Basicas</a></li>
												<li><a href="<?= base_url("provador/editaravancadas")?>">Avançadas</a></li>
											</ul>

											<p>
										
										</div>
									</div>
								</div>

							</div>

						</div>
					</div>

				</div>
				<div class="col s6 m6">
					<div class="panel panel-info">
						<div class="panel-heading">Provador</div>
						<div class="panel-body" style="float: none;">
							<div class="container">
								<input placeholder="Link da roupa" id="loja"
									style="border: 1px solid #337ab7; box-shadow: none" type="text"
									class="validate""> <select class="browser-default" id="tamanho">
									<option value="" disabled selected>Tamanho da roupa</option>
									<option value="38">38</option>
									<option value="36">36</option>
									<option value="32">32</option>
									<option value="P">P</option>
									<option value="M">M</option>
									<option value="G">G</option>
									<option value="GG">GG</option>
									<option value="EG">EG</option>
								</select>
								<div style="margin-top: 10px; margin-bottom: 10px;" id="provar"
									class="btn center blue">Provar</div>
								<div class="result" hidden>
									<ul class="collection">
										<li class="collection-item"><b>Loja:</b><span
											class="secondary-content" id="host"></span></li>
										<li class="collection-item"><b>Status:</b><span
											class="secondary-content"><span id="status"
												class="green-text">Conhecido</span></span></li>
										<li class="collection-item"><b>Match:</b></li>
									</ul>
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
							<b>Suas Medidas</b>
								<ul class="collection" id="minhas_medidas_match" hidden>
									<li class="collection-item">Altura<span
										class="secondary-content"><?= $medida->height?> cm</span></li>
									<li class="collection-item">Peso<span class="secondary-content"><?= $medida->weight?> cm</span></li>
									<li class="collection-item">Ombro<span
										class="secondary-content"><?= $medida->shoulder?> cm</span></li>
									<li class="collection-item">Busto<span
										class="secondary-content"><?= $medida->bust?> cm</span></li>
									<li class="collection-item">Cintura<span
										class="secondary-content"><?= $medida->waist?> cm</span></li>
									<li class="collection-item">Bra&ccedil;o<span
										class="secondary-content"><?= $medida->arm?> cm</span></li>
									<li class="collection-item">Quadril<span
										class="secondary-content"><?= $medida->hip?> cm</span></li>
									<li class="collection-item">Perna<span
										class="secondary-content"><?= $medida->legs?> cm</span></li>
									<li class="collection-item"><a href="#" id="backpanelmedidas">Voltar</a></li>
								</ul>
							
							</div>
							<div class="col-sm-6 col-md-6">
							<b>Medidas da roupa</b>
								<ul class="collection" id="roupas_medidas_match" hidden>
									<li class="collection-item">Altura<span
										class="secondary-content"><span id="roupa_altura"></span> cm</span></li>
									<li class="collection-item">Peso<span class="secondary-content"><?= $medida->weight?> cm</span></li>
									<li class="collection-item">Ombro<span
										class="secondary-content"><span id="roupa_ombro"></span> cm</span></li>
									<li class="collection-item">Busto<span
										class="secondary-content"><span id="roupa_busto"></span> cm</span></li>
									<li class="collection-item">Cintura<span
										class="secondary-content"><span id="roupa_cintura"></span> cm</span></li>
									<li class="collection-item">Bra&ccedil;o<span
										class="secondary-content"><span id="roupa_braco"></span> cm</span></li>
									<li class="collection-item">Quadril<span
										class="secondary-content"><span id="roupa_quadril"></span> cm</span></li>
									<li class="collection-item">Perna<span
										class="secondary-content"><span id="roupa_perna"></span> cm</span></li>
								</ul>
							</div>



						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<script>
$(document).ready(function(){
	load();
});

function load(){
	var a = []; // Create a new empty array.
	a[0] = 'Tirando medidas';
	a[1] = 'Arrumando o espelho';   // Perfectly legal JavaScript that resizes the array.
	a[2] = 'Calculando Body Id';   // Perfectly legal JavaScript that resizes the array.

    i = -1;
    j= 0;
    var loader = (function f(){
        i = (i + 1) % a.length;
        $('#load').text(a[ i ]);
         
         j++;
         if(j>2){
			openApp();
             }
        setTimeout(f, 1000);
     })();
}
function openApp(){
	
    
    $('#load_panel').fadeOut(2000);
    $('#load_panel').hide();
    $('#panel1').fadeIn(2000);
}
$("#ver_medidas").click(function(){
	$("#panel_medidas").fadeOut(function(){
		$("#minhas_medidas").show();
		});
	
});
$("#backpanelmedidas").click(function(){
	$("#minhas_medidas").fadeOut(function(){
		$("#panel_medidas").show();
		});
	
});
$("#provar").click(function(){
	$.ajax({
	    url: "<?=base_url("api/host/") ?>",
	    type: "POST",
	    data: "host="+$("#loja").val()+"&medida="+$("#tamanho").val(),
	    dataType: "html"

	}).done(function(resposta) {
		var obj = JSON.parse(resposta);
		$("#host").text(obj.url);
		if(obj.exist == true){
			$("#status").removeClass();
			$("#status").text("Conhecido");
			$("#status").addClass("green-text");
			}else{
				$("#status").removeClass();
				$("#status").addClass("red-text");
				$("#status").text("Não encontrado");
			}
		$(".result").fadeIn();
		$.ajax({
		    url: "<?=base_url("api/match/") ?>",
		    type: "POST",
		    data: "host="+$("#loja").val()+"&medida="+$("#tamanho").val(),
		    dataType: "html"

		}).done(function(resposta) {
			console.log(resposta);
			var obj = JSON.parse(resposta);
			console.log(obj.medidas[0].ombro);
			var altura = obj.medidas[0].altura;
			var cintura = obj.medidas[0].cintura;
			var ombro = obj.medidas[0].ombro;
			var quadril = obj.medidas[0].quadril;
			var braco = obj.medidas[0].cintura;
			var busto = obj.medidas[0].busto;
			var perna = obj.medidas[0].perna;
			$("#roupa_altura").text(altura);
			$("#roupa_cintura").text(cintura);
			$("#roupa_ombro").text(ombro);
			$("#roupa_quadril").text(quadril);
			$("#roupa_busto").text(busto);
			$("#roupa_braco").text(braco);
			$("#roupa_perna").text(perna);
			$("#minhas_medidas_match").show();
			$("#roupas_medidas_match").show();
			
		}).fail(function(jqXHR, textStatus ) {
		    console.log("Request failed: " + textStatus);

		})
	    console.log(resposta);

	}).fail(function(jqXHR, textStatus ) {
	    console.log("Request failed: " + textStatus);

	}).always(function() {
	    console.log("completou");
	});
});
</script>