<?php

defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
use PHPHtmlParser\Dom;
class Provador extends CI_Controller {
	public function index() {
		$this->is_logged ();
		if ($this->User->have_bodyId ( $this->session->id )) {
			header ( "Location:" . base_url ( 'provador/virtual' ) );
		}
		header(base_url("provador/cadastro"));
	}
	public function cadastro($id) {
		$this->is_logged ();
		
		if ($this->input->post ()) {
			if ($this->input->post ( "step1" ) == "true") {
				$data = $this->input->post ();
				unset ( $data ["step1"] );
				$this->User->inserirMedidas ( "body_id", $this->session->id, 1, $data );
				$this->redirecionaProximo ( ( int ) $this->uri->segment ( '3' ) + 1 );
			}
			if ($this->input->post ( "step2" ) == "true") {
				$data = $this->input->post ();
				unset ( $data ["step2"] );
				$this->User->inserirMedidas ( "advanced_measurements", $this->session->id, 2, $data );
				$this->redirecionaProximo ( ( int ) $this->uri->segment ( '3' ) + 1 );
			}
			if ($this->input->post ( "step3" ) == "true") {
				$data = $this->input->post ();
				unset ( $data ["step3"] );
				@$tipo = $this->input->post ( "disturb" );
				$valores = '';
				foreach ( $tipo as $k => $v ) {
					$valores .= $v;
					$valores .= ' ';
				}
				$valores = trim ( $valores );
				$this->User->inserirMedidas ( "disturb", $this->session->id, 3, $valores );
				$this->redirecionaProximo ( ( int ) $this->uri->segment ( '3' ) + 1 );
			}
		}
		$infoPage ['nomePage'] = 'Provador';
		$this->load->view ( 'default/info-page.php', $infoPage );
		$this->load->view ( 'default/includes-css.php' );
		$this->load->view ( 'default/include-js.php' );
		$this->load->view ( 'default/nav.php', $this->Auth->are_logged () );
		switch ($id) {
			case 1 :
				$this->load->view ( 'provador/cadastro-medidas.php' );
				$this->load->view ( 'default/rodape.php' );
				break;
			
			case 2 :
				$this->load->view ( 'provador/cadastro-medidas-2.php' );
				$this->load->view ( 'default/rodape.php' );
				break;
			case 3 :
				$this->load->view ( 'provador/cadastro-medidas-3.php' );
				$this->load->view ( 'default/rodape.php' );
				break;
			case 4 :
				header ( "Location:" . base_url ( 'provador/virtual' ) );
				break;
		}
	}
	public function redirecionaProximo($idPaginaAtual) {
		$this->is_logged ();
		header ( "Location:" . base_url () . "provador/cadastro/" . $idPaginaAtual );
	}
	public function editarbasicas(){
		if ($this->input->post ()) {
			if ($this->input->post ( "step1" ) == "true") {
				$data = $this->input->post ();
				unset ( $data ["step1"] );
				$this->User->atualizarMedidas ( $this->session->id,"body_id",$data );
				$modal['title'] = 'Ok!';
				$modal['text'] = 'Medidas alteradas!';
				$this->load->view('default/include-js.php');
				$this->load->view('alerts/modal',$modal);
			}
		}
		$data = $this->User->return_medidas($this->Auth->are_logged()["id"])[0];
		$infoPage ['nomePage'] = 'Editar';
		$this->load->view ( 'default/info-page.php', $infoPage );
		$this->load->view ( 'default/includes-css.php' );
		$this->load->view ( 'default/include-js.php' );
		$this->load->view ( 'default/nav.php', $this->Auth->are_logged () );
		$this->load->view ( 'provador/cadastro-medidas.php',$data );
		$this->load->view ( 'default/rodape.php' );
		
	}
	public function editaravancadas(){
		if ($this->input->post ()) {
			if ($this->input->post ( "step2" ) == "true") {
				$data = $this->input->post ();
				unset ( $data ["step2"] );
				$this->User->atualizarMedidasAvancadas ( $this->session->id,"advanced_measurements",$data );
				$modal['title'] = 'Ok!';
				$modal['text'] = 'Medidas alteradas!';
				$this->load->view('default/include-js.php');
				$this->load->view('alerts/modal',$modal);
			}
		}
		$data = $this->User->return_advanced_medidas($this->Auth->are_logged()["id"])[0];
		$infoPage ['nomePage'] = 'Editar';
		$this->load->view ( 'default/info-page.php', $infoPage );
		$this->load->view ( 'default/includes-css.php' );
		$this->load->view ( 'default/include-js.php' );
		$this->load->view ( 'default/nav.php', $this->Auth->are_logged () );
		$this->load->view ( 'provador/cadastro-medidas-2.php',$data );
		$this->load->view ( 'default/rodape.php' );
	
	}
	public function virtual() {
		$this->is_logged ();
		$infoPage ['nomePage'] = 'Provador virtual';
		$data['medida'] = $this->User->return_medidas ( $this->session->id )[0];
		
		$this->load->view ( 'default/info-page.php', $infoPage );
		$this->load->view ( 'default/includes-css.php' );
		$this->load->view ( 'default/include-js.php' );
		$this->load->view ( 'default/nav.php', $this->Auth->are_logged () );
		$this->load->view ( 'provador/app/index.php', $data );
		$this->load->view ( 'default/rodape.php' );
	}
	public function is_logged() {
		if (! isset ( $this->Auth->are_logged () ['id'] )) {
			header ( "Location:" . base_url ( 'login' ) );
		}
	}
	public function test(){
	
		
		$dom = new Dom();
		$dom->load('<div class="all"><p>Hey bro, <a href="google.com">click here</a><br /> :)</p></div>');
		$a = $dom->find('a')[0];
		echo $a->text;
	}
}