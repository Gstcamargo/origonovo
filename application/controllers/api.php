<?php
class api extends CI_Controller {
	public function index() {
		parent::__construct ();
	}
	public function host() {
		$url = parse_url ( $this->input->post ( "host" ) );
		
		$this->load->model("Provar");
		$exist = $this->Provar->exist_host($url["host"],$this->input->post ( "medida" ));
		$return = array("url"=> $url["host"],"exist" => $exist );
		echo json_encode ($return);
	}
	public function match() {
		$url = parse_url ( $this->input->post ( "host" ) );
		$this->load->model("Provar");
		$exist = $this->Provar->exist_host($url["host"],$this->input->post ( "medida" ));
		$return = array("medidas"=> $this->Provar->get_host($url["host"],$this->input->post ( "medida" )));
		echo json_encode ($return);
	}
	public function cadastrodemarca(){
		if($this->input->post("host")){
			$this->load->model("Model_admin");
			$data = $this->input->post();
			$this->Model_admin->insert_host($data);
		}
	}
}