<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller{
	public function index()
	{
		if(!isset($this->session->id)){
		
		if($this->input->post()){
			
			if($this->Auth->logar($this->input->post('email'),$this->input->post('senha'))){
				if($this->Auth->get_type()==2){
					header("Location:".base_url()."admin");
				}
				header("Location:".base_url()."provador");
			}else{
				$modal['title'] = 'Ops';
				$modal['text'] = 'Usuario ou senha incorretos';
				$this->load->view('default/include-js.php');
				$this->load->view('alerts/modal',$modal);
			}
		}
		$infoPage['nomePage'] = 'Login';
		$this->load->view('default/info-page.php',$infoPage);
		$this->load->view('default/includes-css.php');
		$this->load->view('default/nav.php',$this->Auth->are_logged());
		$this->load->view('login');
		$this->load->view('default/rodape.php');	
		}else{
			$modal['alert_nome'] = 'Ops';
			$modal['alert_text'] = 'Você ja está logado';
			$infoPage['nomePage'] = 'Ops';
			$this->load->view('default/info-page.php',$infoPage);
			$this->load->view('default/includes-css.php');
			$this->load->view('default/nav.php',$this->Auth->are_logged());
			$this->load->view('alerts/pagealert',$modal);
			$this->load->view('default/rodape.php');
		}
	}
	public function logout(){
		$this->Auth->logout();
		header("Location:".base_url('login'));
	}
}