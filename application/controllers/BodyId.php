<?php
class BodyId extends CI_Controller {
	
	public function index() {
		$infoPage['nomePage'] = 'Login';
		$this->load->view('default/info-page.php',$infoPage);
		$this->load->view('default/includes-css.php');
		$this->load->view('default/nav.php',$this->Auth->are_logged());
		$this->load->view('login');
		$this->load->view('default/rodape.php');
	}
}
