<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$infoPage['nomePage'] = 'Home';
		$this->load->view('default/info-page.php',$infoPage);
		$this->load->view('default/includes-css.php');
		$this->load->view('default/nav.php',$this->Auth->are_logged());
		$this->load->view('index');
		$this->load->view('default/rodape.php');
	}

}
