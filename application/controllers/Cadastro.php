<?php


defined('BASEPATH') OR exit('No direct script access allowed');
class Cadastro extends CI_Controller{
	public function index()
	{
		
		if($this->input->post('cadastro')){
			$this->load->model('User');
			$dados = $this->input->post();
			unset ($dados['cadastro']);
			if($this->User->cadastro($dados)){
				$this->Auth->logar($dados['email'],$dados['password']);
				header(base_url('provador/cadastro/1'));
			}else{
				$modal['title'] = 'Ops';
				$modal['text'] = 'Usuario ja cadastrado';
				$this->load->view('default/include-js.php');
				$this->load->view('alerts/modal',$modal);
			}
			
		}
		$infoPage['nomePage'] = 'Cadastro';
		$this->load->view('default/info-page.php',$infoPage);
		$this->load->view('default/includes-css.php');
		$this->load->view('default/nav.php',$this->Auth->are_logged());
		$this->load->view('cadastro');
		$this->load->view('default/rodape.php');
	}
}