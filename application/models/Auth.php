<?php
class Auth extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * @param unknown $dados
	 * @return boolean
	 */
	public function logar($login,$senha) {
		$this->load->database();
		
		$query = $this->db->get_where ( 'users', array (
				'email' => $login,
				'password' => $senha
		) );
		
		 if ($query->num_rows() > 0) {
			$this->session->id = $query->result()[0]->id;
			$this->session->name = $query->result()[0]->name;
			return true;
		}else{
			return false;
		}
		
	}
	public function get_type(){
		if($this->are_logged()){
			$query = $this->db->get_where ( 'users', array (
					'id' => $this->are_logged()["id"]
			) );
			return $query->result()[0]->type;
		}else{
			return false;
		}
	}
	public function are_logged(){
		if(isset($this->session->id)){
			$dados['id'] = $this->session->id;
			$dados['nome'] = $this->session->name;
			return $dados;
		}else{
			return false;
		}
	}
	public function logout(){
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('nome');
		return true;
	}
};