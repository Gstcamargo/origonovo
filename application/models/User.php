<?php
class User extends CI_Model {
	var $name;
	public function __construct()
	{
		$this->load->database();
		parent::__construct();
	}
	
	public function cadastro($dados) {
		
		
		$query = $this->db->get_where ( 'users', array (
				'email' => $dados ['email'] 
		) );
		
		 if ($query->num_rows() > 0) {
			return false;
		}else{
			$this->db->insert ( 'users', $dados );
			return true;
		}
		
	}
	public function inserirMedidas($table,$idUser,$step,$data){
	
		if($step==1){
			$info = array("user_id"=>$idUser);
			$result = array_merge($info,$data);
			$this->db->insert($table,$result);
		}
		if($step==2){
			$info = array("idBody"=>$idUser);
			$result = array_merge($info,$data);
			$this->db->insert($table,$result);
		}
		if($step==3){
			$info = array("body_id"=>$idUser,"disturb_measure"=>$data);
			$this->db->insert($table,$info);
		}
	}
	public function have_bodyId($id){
		$query = $this->db->get_where ( "body_id", array (
				'user_id' => $id
		) );
		
		if ($query->num_rows() > 0) {
			return true;
		}else{
			return false;
		}
	}
	public function return_medidas($id){
		$query = $this->db->get_where ( "body_id", array (
				'user_id' => $id
		) );
		return $query->result();
	}
	public function return_advanced_medidas($id){
		$query = $this->db->get_where ( "advanced_measurements", array (
				'idBody' => $id
		) );
		return $query->result();
	}
	public function atualizarMedidas($id, $table,$data){
		$this->db->where('user_id', $id);
		$this->db->update($table, $data);
		
	}
	public function atualizarMedidasAvancadas($id, $table,$data){
		$this->db->where('idBody', $id);
		$this->db->update($table, $data);
	
	}
};