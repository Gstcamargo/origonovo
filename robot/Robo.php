<?php
require "vendor/autoload.php";
use PHPHtmlParser\Dom;
class Robo {
	// Url do site a ser buscado
	var $url;
	var $host;
	var $hostTmp;
	var $hostSuportados = [
			'RENNER' => 'www.lojasrenner.com.br'
	];
	function __construct() {
	}

	/*
	 * Seta a Url
	 */
	public function setUrl($url) {
		$this->url = $url;
	}
	public function crawler() {
		$this->getAction ();
		return $this->getCrawlers ();
	}
	/**
	 * Pega o host da URL
	 *
	 * @return boolean
	 */
	private function getHost() {
		try {
			$urlQuebrada = parse_url ( $this->url );
			$this->host = $urlQuebrada ['host'];
			return $this->host;
		} catch ( Exception $e ) {
			return false;
		}
	}
	/**
	 * Verifica se existe a função para buscar o Link;
	 *
	 * @return unknown
	 */
	private function getAction() {
		$result = array_search ( $this->getHost (), $this->hostSuportados );
		if ($result != false) {
			$this->hostTmp = $result;
		} else {
			return false;
		}
	}
	private function getCrawlers() {
		switch ($this->hostTmp) {
			case 'RENNER' :
				return $this->getRenner ();
		}
	}
	private function extractNumber($str) {
		preg_match_all ( '!\d+!', $str, $matches );
		return implode ( '', $matches [0] );
	}
	// Daqui pra baixo só as funções referentes as lojas
	private function getRenner() {
		$dom = new Dom ();
		$dom->loadFromUrl ( $this->url );
		$a = $dom->find ( '.AllDescripP' ) [0];
		$parteDeMedidas = $a->find ( 'p' ) [0]->find ( 'p' );
		$altura = (float) $this->extractNumber($parteDeMedidas [3]->text);
		$busto = (float)  $this->extractNumber($parteDeMedidas [4]->text);
		$cintura = (float) $this->extractNumber($parteDeMedidas [5]->text);
		$quadril = (float) $this->extractNumber($parteDeMedidas [6]->text);
		$return = array (
				'altura' => $altura,
				'cintura' => $cintura,
				'busto' => $busto,
				'quadril' => $quadril
		);
		return $return;
	}
}

?>