<meta charset="UTF-8">
<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">

<script type="text/javascript"
	src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<!-- Compiled and minified JavaScript -->
<title>Notas</title>

<script
	src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
<link href="http://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<div class="container">
	<h1>Observações</h1>
	<p class="flow-text">Bom fique responsavel de fazer o Crawler do sites.
		Porem tive algumas "pedrinhas no caminho"</p>
	<p>Clique no item da lista pra mais detalhes</p>
	<ul class="collapsible" data-collapsible="accordion">
		<li>
			<div class="collapsible-header">
				<i class="material-icons">view_module</i>Alguns sites usam tabelas
			</div>
			<div class="collapsible-body center">
				<p>Alguns sites ultilizam uma tabela enorme com todas as medidas, de
					todas as roupas, meio que uma medida padrão pra todas as roupas
					como essa abaixo</p>
				<img
					src="http://cortandoecosturando.com/Cal%E7a_TP_masc/medidas%20masculina.jpg"
					height="300px" />
				<p class="green-text">Talvez seria interessante, ter essas medidas
					salva no sistema, mais se a ideia é o usuario ter a sensação de
					estar provando virtualmente com as medidas, desconsidera</p>
			</div>
		</li>
		<li>
			<div class="collapsible-header">
				<i class="material-icons">image</i>Outros Imagens
			</div>
			<div class="collapsible-body">
				<p>A mesma problematica acima, só que com imagens, muitas das vezes
					eles mesmos desenham como o logo da loja, o que torna inviavel o
					acesso</p>
			</div>
		</li>
		<li>
			<div class="collapsible-header">
				<i class="material-icons">toc</i> Outros Paragrafos
			</div>
			<div class="collapsible-body">
				<p>Alguns sites usam tags simples como a tag p ou h1</p>
			</div>
		</li>
		<li>
			<div class="collapsible-header">
				<i class="material-icons">done</i> Solução encontrada
			</div>
			<div class="collapsible-body">
				<p>A solução que eu encontrei, foi como programei esse robozinho,
					ele possui um vetor com os hosts, e executaria um crawler diferente
					pra cada site</p>
			</div>
		</li>
		<li>
			<div class="collapsible-header">
				<i class="material-icons">code</i> Viabilidade (Minha analise)
			</div>
			<div class="collapsible-body">
				<p>O maior problema, é que teria que possuir um metodo para cada
					loja, como o Robo que eu fiz para demonstrar essa situação, ele ja
					é capaz de indetificar o host e fazer o crawler no site da Renner,
					porem só consegui pegar as medias que ficam embaixo que são as da
					modelo, ja que a tabela completa é uma imagem</p>
			</div>
		</li>
	</ul>

	<div class="row center">
		<div class="col s12 m6 offset-m3 card">
			<form action="" method="post">
				<h1>Robo</h1>
				<p>
					O robo só fuciona com as blusas da renner, tentei fazer o codigo
					dele mais versatil possivel, examinei o site da renner e fiz um
					crawler e criei o metodo, o é bem simples, ele apenas indentifica o
					host, vê se existe um metodo para esse host e chama se possivel,
					usei uma Library externa pra indentificar o html com mais
					facilidade, vá no site da renner pegue o link de blusa e teste aqui
					ou só <a
						href="http://www.lojasrenner.com.br/p/blusa-jeans-ombro-a-ombro-540890614-540890665"
						target="_blank">Clique aqui</a> e copie
				</p>
				<input name="link"></input>
				<button class="btn">Requisitar</button>
				<br>
			</form>

						<?php
						require 'Robo.php';
						if (isset ( $_POST ['link'] ) && $_POST ['link'] != "") {
							$robo = new Robo ();
							$robo->setUrl ( $_POST ['link'] );
							$result = $robo->crawler ();
							
							?>
Altura:  <input placeholder="altura" value="<?= $result['altura'] ?>"></input><br>
			Busto: <input placeholder="busto" value="<?= $result['busto'] ?>"></input><br>
			Cintura: <input placeholder="cintura"
				value="<?= $result['cintura'] ?>"></input><br> Quadril: <input
				placeholder="quadril" value="<?= $result['quadril'] ?>"></input><br>
<?php }?>
</div>
	</div>
</div>

<script>  
$(document).ready(function(){
    $('.collapsible').collapsible({
        accordion : false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
      });
 </script>