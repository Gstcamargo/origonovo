$(document).ready(function(){
        $('select#sel1').change(function(){
        	var a = $('#sel1 :selected').val();
        	if(a == "vestido")
        	{
				$("#selectneckpp").css("display","none");
				$("#selectshoulderpp").css("display","block");
				$("#selectbustpp").css("display","block");
				$("#selecttrunkpp").css("display","block");
				$("#selectwaistpp").css("display","block");
				$("#selecthippp").css("display","block");
				$("#selectarmpp").css("display","block");
				$("#selectlegspp").css("display","none");
				$("#selecthandlepp").css("display","none");
				$("#selecthookpp").css("display","none");
				$("#selectthighpp").css("display","none");
				$("#selectcalfpp").css("display","none");
				$("#selectopeningBarpp").css("display","none");
				$("#selectbarWidthpp").css("display","block");
				$("#selectlengthpp").css("display","block");

				$("#selectneckp").css("display","none");
				$("#selectshoulderp").css("display","block");
				$("#selectbustp").css("display","block");
				$("#selecttrunkp").css("display","block");
				$("#selectwaistp").css("display","block");
				$("#selecthipp").css("display","block");
				$("#selectarmp").css("display","block");
				$("#selectlegsp").css("display","none");
				$("#selecthandlep").css("display","none");
				$("#selecthookp").css("display","none");
				$("#selectthighp").css("display","none");
				$("#selectcalfp").css("display","none");
				$("#selectopeningBarp").css("display","none");
				$("#selectbarWidthp").css("display","block");
				$("#selectlengthp").css("display","block");

				$("#selectneckm").css("display","none");
				$("#selectshoulderm").css("display","block");
				$("#selectbustm").css("display","block");
				$("#selecttrunkm").css("display","block");
				$("#selectwaistm").css("display","block");
				$("#selecthipm").css("display","block");
				$("#selectarmm").css("display","block");
				$("#selectlegsm").css("display","none");
				$("#selecthandlem").css("display","none");
				$("#selecthookm").css("display","none");
				$("#selectthighm").css("display","none");
				$("#selectcalfm").css("display","none");
				$("#selectopeningBarm").css("display","none");
				$("#selectbarWidthm").css("display","block");
				$("#selectlengthm").css("display","block");

				$("#selectneckg").css("display","none");
				$("#selectshoulderg").css("display","block");
				$("#selectbustg").css("display","block");
				$("#selecttrunkg").css("display","block");
				$("#selectwaistg").css("display","block");
				$("#selecthipg").css("display","block");
				$("#selectarmg").css("display","block");
				$("#selectlegsg").css("display","none");
				$("#selecthandleg").css("display","none");
				$("#selecthookg").css("display","none");
				$("#selectthighg").css("display","none");
				$("#selectcalfg").css("display","none");
				$("#selectopeningBarg").css("display","none");
				$("#selectbarWidthg").css("display","block");
				$("#selectlengthg").css("display","block");

				$("#selectneckgg").css("display","none");
				$("#selectshouldergg").css("display","block");
				$("#selectbustgg").css("display","block");
				$("#selecttrunkgg").css("display","block");
				$("#selectwaistgg").css("display","block");
				$("#selecthipgg").css("display","block");
				$("#selectarmgg").css("display","block");
				$("#selectlegsgg").css("display","none");
				$("#selecthandlegg").css("display","none");
				$("#selecthookgg").css("display","none");
				$("#selectthighgg").css("display","none");
				$("#selectcalfgg").css("display","none");
				$("#selectopeningBargg").css("display","none");
				$("#selectbarWidthgg").css("display","block");
				$("#selectlengthgg").css("display","block");

				$("#selectneckxg").css("display","none");
				$("#selectshoulderxg").css("display","block");
				$("#selectbustxg").css("display","block");
				$("#selecttrunkxg").css("display","block");
				$("#selectwaistxg").css("display","block");
				$("#selecthipxg").css("display","block");
				$("#selectarmxg").css("display","block");
				$("#selectlegsxg").css("display","none");
				$("#selecthandlexg").css("display","none");
				$("#selecthookxg").css("display","none");
				$("#selectthighxg").css("display","none");
				$("#selectcalfxg").css("display","none");
				$("#selectopeningBarxg").css("display","none");
				$("#selectbarWidthxg").css("display","block");
				$("#selectlengthxg").css("display","block");

				$("#selectneckxxg").css("display","none");
				$("#selectshoulderxxg").css("display","block");
				$("#selectbustxxg").css("display","block");
				$("#selecttrunkxxg").css("display","block");
				$("#selectwaistxxg").css("display","block");
				$("#selecthipxxg").css("display","block");
				$("#selectarmxxg").css("display","block");
				$("#selectlegsxxg").css("display","none");
				$("#selecthandlexxg").css("display","none");
				$("#selecthookxxg").css("display","none");
				$("#selectthighxxg").css("display","none");
				$("#selectcalfxxg").css("display","none");
				$("#selectopeningBarxxg").css("display","none");
				$("#selectbarWidthxxg").css("display","block");
				$("#selectlengthxxg").css("display","block");

				$("#selectneckother").css("display","none");
				$("#selectshoulderother").css("display","block");
				$("#selectbustother").css("display","block");
				$("#selecttrunkother").css("display","block");
				$("#selectwaistother").css("display","block");
				$("#selecthipother").css("display","block");
				$("#selectarmother").css("display","block");
				$("#selectlegsother").css("display","none");
				$("#selecthandleother").css("display","none");
				$("#selecthookother").css("display","none");
				$("#selectthighother").css("display","none");
				$("#selectcalfother").css("display","none");
				$("#selectopeningBarother").css("display","none");
				$("#selectbarWidthother").css("display","block");
				$("#selectlengthother").css("display","block");

			}
        	if(a == "macacao")
        	{
				$("#selectneckpp").css("display","none");
				$("#selectshoulderpp").css("display","block");
				$("#selectbustpp").css("display","block");
				$("#selecttrunkpp").css("display","none");
				$("#selectwaistpp").css("display","block");
				$("#selecthippp").css("display","block");
				$("#selectarmpp").css("display","block");
				$("#selectlegspp").css("display","block");
				$("#selecthandlepp").css("display","none");
				$("#selecthookpp").css("display","block");
				$("#selectthighpp").css("display","block");
				$("#selectcalfpp").css("display","block");
				$("#selectopeningBarpp").css("display","none");
				$("#selectbarWidthpp").css("display","none");
				$("#selectlengthpp").css("display","none");

				$("#selectneckp").css("display","none");
				$("#selectshoulderp").css("display","block");
				$("#selectbustp").css("display","block");
				$("#selecttrunkp").css("display","none");
				$("#selectwaistp").css("display","block");
				$("#selecthipp").css("display","block");
				$("#selectarmp").css("display","block");
				$("#selectlegsp").css("display","block");
				$("#selecthandlep").css("display","none");
				$("#selecthookp").css("display","block");
				$("#selectthighp").css("display","block");
				$("#selectcalfp").css("display","block");
				$("#selectopeningBarp").css("display","none");
				$("#selectbarWidthp").css("display","none");
				$("#selectlengthp").css("display","none");

				$("#selectneckm").css("display","none");
				$("#selectshoulderm").css("display","block");
				$("#selectbustm").css("display","block");
				$("#selecttrunkm").css("display","none");
				$("#selectwaistm").css("display","block");
				$("#selecthipm").css("display","block");
				$("#selectarmm").css("display","block");
				$("#selectlegsm").css("display","block");
				$("#selecthandlem").css("display","none");
				$("#selecthookm").css("display","block");
				$("#selectthighm").css("display","block");
				$("#selectcalfm").css("display","block");
				$("#selectopeningBarm").css("display","none");
				$("#selectbarWidthm").css("display","none");
				$("#selectlengthm").css("display","none");

				$("#selectneckg").css("display","none");
				$("#selectshoulderg").css("display","block");
				$("#selectbustg").css("display","block");
				$("#selecttrunkg").css("display","none");
				$("#selectwaistg").css("display","block");
				$("#selecthipg").css("display","block");
				$("#selectarmg").css("display","block");
				$("#selectlegsg").css("display","block");
				$("#selecthandleg").css("display","none");
				$("#selecthookg").css("display","block");
				$("#selectthighg").css("display","block");
				$("#selectcalfg").css("display","block");
				$("#selectopeningBarg").css("display","none");
				$("#selectbarWidthg").css("display","none");
				$("#selectlengthg").css("display","none");

				$("#selectneckgg").css("display","none");
				$("#selectshouldergg").css("display","block");
				$("#selectbustgg").css("display","block");
				$("#selecttrunkgg").css("display","none");
				$("#selectwaistgg").css("display","block");
				$("#selecthipgg").css("display","block");
				$("#selectarmgg").css("display","block");
				$("#selectlegsgg").css("display","block");
				$("#selecthandlegg").css("display","none");
				$("#selecthookgg").css("display","block");
				$("#selectthighgg").css("display","block");
				$("#selectcalfgg").css("display","block");
				$("#selectopeningBargg").css("display","none");
				$("#selectbarWidthgg").css("display","none");
				$("#selectlengthgg").css("display","none");

				$("#selectneckxg").css("display","none");
				$("#selectshoulderxg").css("display","block");
				$("#selectbustxg").css("display","block");
				$("#selecttrunkxg").css("display","none");
				$("#selectwaistxg").css("display","block");
				$("#selecthipxg").css("display","block");
				$("#selectarmxg").css("display","block");
				$("#selectlegsxg").css("display","block");
				$("#selecthandlexg").css("display","none");
				$("#selecthookxg").css("display","block");
				$("#selectthighxg").css("display","block");
				$("#selectcalfxg").css("display","block");
				$("#selectopeningBarxg").css("display","none");
				$("#selectbarWidthxg").css("display","none");
				$("#selectlengthxg").css("display","none");

				$("#selectneckxxg").css("display","none");
				$("#selectshoulderxxg").css("display","block");
				$("#selectbustxxg").css("display","block");
				$("#selecttrunkxxg").css("display","none");
				$("#selectwaistxxg").css("display","block");
				$("#selecthipxxg").css("display","block");
				$("#selectarmxxg").css("display","block");
				$("#selectlegsxxg").css("display","block");
				$("#selecthandlexxg").css("display","none");
				$("#selecthookxxg").css("display","block");
				$("#selectthighxxg").css("display","block");
				$("#selectcalfxxg").css("display","block");
				$("#selectopeningBarxxg").css("display","none");
				$("#selectbarWidthxxg").css("display","none");
				$("#selectlengthxxg").css("display","none");

				$("#selectneckother").css("display","none");
				$("#selectshoulderother").css("display","block");
				$("#selectbustother").css("display","block");
				$("#selecttrunkother").css("display","none");
				$("#selectwaistother").css("display","block");
				$("#selecthipother").css("display","block");
				$("#selectarmother").css("display","block");
				$("#selectlegsother").css("display","block");
				$("#selecthandleother").css("display","none");
				$("#selecthookother").css("display","block");
				$("#selectthighother").css("display","block");
				$("#selectcalfother").css("display","block");
				$("#selectopeningBarother").css("display","none");
				$("#selectbarWidthother").css("display","none");
				$("#selectlengthother").css("display","none");
			}
			if(a == "camisa")
			{
				$("#selectneckpp").css("display","block");
				$("#selectshoulderpp").css("display","block");
				$("#selectbustpp").css("display","block");
				$("#selecttrunkpp").css("display","block");
				$("#selectwaistpp").css("display","block");
				$("#selecthippp").css("display","none");
				$("#selectarmpp").css("display","block");
				$("#selectlegspp").css("display","none");
				$("#selecthandlepp").css("display","block");
				$("#selecthookpp").css("display","none");
				$("#selectthighpp").css("display","none");
				$("#selectcalfpp").css("display","none");
				$("#selectopeningBarpp").css("display","none");
				$("#selectbarWidthpp").css("display","none");
				$("#selectlengthpp").css("display","none");

				$("#selectneckp").css("display","block");
				$("#selectshoulderp").css("display","block");
				$("#selectbustp").css("display","block");
				$("#selecttrunkp").css("display","block");
				$("#selectwaistp").css("display","block");
				$("#selecthipp").css("display","none");
				$("#selectarmp").css("display","block");
				$("#selectlegsp").css("display","none");
				$("#selecthandlep").css("display","block");
				$("#selecthookp").css("display","none");
				$("#selectthighp").css("display","none");
				$("#selectcalfp").css("display","none");
				$("#selectopeningBarp").css("display","none");
				$("#selectbarWidthp").css("display","none");
				$("#selectlengthp").css("display","none");

				$("#selectneckm").css("display","block");
				$("#selectshoulderm").css("display","block");
				$("#selectbustm").css("display","block");
				$("#selecttrunkm").css("display","block");
				$("#selectwaistm").css("display","block");
				$("#selecthipm").css("display","none");
				$("#selectarmm").css("display","block");
				$("#selectlegsm").css("display","none");
				$("#selecthandlem").css("display","block");
				$("#selecthookm").css("display","none");
				$("#selectthighm").css("display","none");
				$("#selectcalfm").css("display","none");
				$("#selectopeningBarm").css("display","none");
				$("#selectbarWidthm").css("display","none");
				$("#selectlengthm").css("display","none");

				$("#selectneckg").css("display","block");
				$("#selectshoulderg").css("display","block");
				$("#selectbustg").css("display","block");
				$("#selecttrunkg").css("display","block");
				$("#selectwaistg").css("display","block");
				$("#selecthipg").css("display","none");
				$("#selectarmg").css("display","block");
				$("#selectlegsg").css("display","none");
				$("#selecthandleg").css("display","block");
				$("#selecthookg").css("display","none");
				$("#selectthighg").css("display","none");
				$("#selectcalfg").css("display","none");
				$("#selectopeningBarg").css("display","none");
				$("#selectbarWidthg").css("display","none");
				$("#selectlengthg").css("display","none");

				$("#selectneckgg").css("display","block");
				$("#selectshouldergg").css("display","block");
				$("#selectbustgg").css("display","block");
				$("#selecttrunkgg").css("display","block");
				$("#selectwaistgg").css("display","block");
				$("#selecthipgg").css("display","none");
				$("#selectarmgg").css("display","block");
				$("#selectlegsgg").css("display","none");
				$("#selecthandlegg").css("display","block");
				$("#selecthookgg").css("display","none");
				$("#selectthighgg").css("display","none");
				$("#selectcalfgg").css("display","none");
				$("#selectopeningBargg").css("display","none");
				$("#selectbarWidthgg").css("display","none");
				$("#selectlengthgg").css("display","none");

				$("#selectneckxg").css("display","block");
				$("#selectshoulderxg").css("display","block");
				$("#selectbustxg").css("display","block");
				$("#selecttrunkxg").css("display","block");
				$("#selectwaistxg").css("display","block");
				$("#selecthipxg").css("display","none");
				$("#selectarmxg").css("display","block");
				$("#selectlegsxg").css("display","none");
				$("#selecthandlexg").css("display","block");
				$("#selecthookxg").css("display","none");
				$("#selectthighxg").css("display","none");
				$("#selectcalfxg").css("display","none");
				$("#selectopeningBarxg").css("display","none");
				$("#selectbarWidthxg").css("display","none");
				$("#selectlengthxg").css("display","none");

				$("#selectneckxxg").css("display","block");
				$("#selectshoulderxxg").css("display","block");
				$("#selectbustxxg").css("display","block");
				$("#selecttrunkxxg").css("display","block");
				$("#selectwaistxxg").css("display","block");
				$("#selecthipxxg").css("display","none");
				$("#selectarmxxg").css("display","block");
				$("#selectlegsxxg").css("display","none");
				$("#selecthandlexxg").css("display","block");
				$("#selecthookxxg").css("display","none");
				$("#selectthighxxg").css("display","none");
				$("#selectcalfxxg").css("display","none");
				$("#selectopeningBarxxg").css("display","none");
				$("#selectbarWidthxxg").css("display","none");
				$("#selectlengthxxg").css("display","none");

				$("#selectneckother").css("display","block");
				$("#selectshoulderother").css("display","block");
				$("#selectbustother").css("display","block");
				$("#selecttrunkother").css("display","block");
				$("#selectwaistother").css("display","block");
				$("#selecthipother").css("display","none");
				$("#selectarmother").css("display","block");
				$("#selectlegsother").css("display","none");
				$("#selecthandleother").css("display","block");
				$("#selecthookother").css("display","none");
				$("#selectthighother").css("display","none");
				$("#selectcalfother").css("display","none");
				$("#selectopeningBarother").css("display","none");
				$("#selectbarWidthother").css("display","none");
				$("#selectlengthother").css("display","none");
			}
			if(a =="camiseta")
			{
				$("#selectneckpp").css("display","none");
				$("#selectshoulderpp").css("display","block");
				$("#selectbustpp").css("display","block");
				$("#selecttrunkpp").css("display","block");
				$("#selectwaistpp").css("display","block");
				$("#selecthippp").css("display","none");
				$("#selectarmpp").css("display","block");
				$("#selectlegspp").css("display","none");
				$("#selecthandlepp").css("display","none");
				$("#selecthookpp").css("display","none");
				$("#selectthighpp").css("display","none");
				$("#selectcalfpp").css("display","none");
				$("#selectopeningBarpp").css("display","none");
				$("#selectbarWidthpp").css("display","none");
				$("#selectlengthpp").css("display","none");

				$("#selectneckp").css("display","none");
				$("#selectshoulderp").css("display","block");
				$("#selectbustp").css("display","block");
				$("#selecttrunkp").css("display","block");
				$("#selectwaistp").css("display","block");
				$("#selecthipp").css("display","none");
				$("#selectarmp").css("display","block");
				$("#selectlegsp").css("display","none");
				$("#selecthandlep").css("display","none");
				$("#selecthookp").css("display","none");
				$("#selectthighp").css("display","none");
				$("#selectcalfp").css("display","none");
				$("#selectopeningBarp").css("display","none");
				$("#selectbarWidthp").css("display","none");
				$("#selectlengthp").css("display","none");

				$("#selectneckm").css("display","none");
				$("#selectshoulderm").css("display","block");
				$("#selectbustm").css("display","block");
				$("#selecttrunkm").css("display","block");
				$("#selectwaistm").css("display","block");
				$("#selecthipm").css("display","none");
				$("#selectarmm").css("display","block");
				$("#selectlegsm").css("display","none");
				$("#selecthandlem").css("display","none");
				$("#selecthookm").css("display","none");
				$("#selectthighm").css("display","none");
				$("#selectcalfm").css("display","none");
				$("#selectopeningBarm").css("display","none");
				$("#selectbarWidthm").css("display","none");
				$("#selectlengthm").css("display","none");

				$("#selectneckg").css("display","none");
				$("#selectshoulderg").css("display","block");
				$("#selectbustg").css("display","block");
				$("#selecttrunkg").css("display","block");
				$("#selectwaistg").css("display","block");
				$("#selecthipg").css("display","none");
				$("#selectarmg").css("display","block");
				$("#selectlegsg").css("display","none");
				$("#selecthandleg").css("display","none");
				$("#selecthookg").css("display","none");
				$("#selectthighg").css("display","none");
				$("#selectcalfg").css("display","none");
				$("#selectopeningBarg").css("display","none");
				$("#selectbarWidthg").css("display","none");
				$("#selectlengthg").css("display","none");

				$("#selectneckgg").css("display","none");
				$("#selectshouldergg").css("display","block");
				$("#selectbustgg").css("display","block");
				$("#selecttrunkgg").css("display","block");
				$("#selectwaistgg").css("display","block");
				$("#selecthipgg").css("display","none");
				$("#selectarmgg").css("display","block");
				$("#selectlegsgg").css("display","none");
				$("#selecthandlegg").css("display","none");
				$("#selecthookgg").css("display","none");
				$("#selectthighgg").css("display","none");
				$("#selectcalfgg").css("display","none");
				$("#selectopeningBargg").css("display","none");
				$("#selectbarWidthgg").css("display","none");
				$("#selectlengthgg").css("display","none");

				$("#selectneckxg").css("display","none");
				$("#selectshoulderxg").css("display","block");
				$("#selectbustxg").css("display","block");
				$("#selecttrunkxg").css("display","block");
				$("#selectwaistxg").css("display","block");
				$("#selecthipxg").css("display","none");
				$("#selectarmxg").css("display","block");
				$("#selectlegsxg").css("display","none");
				$("#selecthandlexg").css("display","none");
				$("#selecthookxg").css("display","none");
				$("#selectthighxg").css("display","none");
				$("#selectcalfxg").css("display","none");
				$("#selectopeningBarxg").css("display","none");
				$("#selectbarWidthxg").css("display","none");
				$("#selectlengthxg").css("display","none");

				$("#selectneckxxg").css("display","none");
				$("#selectshoulderxxg").css("display","block");
				$("#selectbustxxg").css("display","block");
				$("#selecttrunkxxg").css("display","block");
				$("#selectwaistxxg").css("display","block");
				$("#selecthipxxg").css("display","none");
				$("#selectarmxxg").css("display","block");
				$("#selectlegsxxg").css("display","none");
				$("#selecthandlexxg").css("display","none");
				$("#selecthookxxg").css("display","none");
				$("#selectthighxxg").css("display","none");
				$("#selectcalfxxg").css("display","none");
				$("#selectopeningBarxxg").css("display","none");
				$("#selectbarWidthxxg").css("display","none");
				$("#selectlengthxxg").css("display","none");

				$("#selectneckother").css("display","none");
				$("#selectshoulderother").css("display","block");
				$("#selectbustother").css("display","block");
				$("#selecttrunkother").css("display","block");
				$("#selectwaistother").css("display","block");
				$("#selecthipother").css("display","none");
				$("#selectarmother").css("display","block");
				$("#selectlegsother").css("display","none");
				$("#selecthandleother").css("display","none");
				$("#selecthookother").css("display","none");
				$("#selectthighother").css("display","none");
				$("#selectcalfother").css("display","none");
				$("#selectopeningBarother").css("display","none");
				$("#selectbarWidthother").css("display","none");
				$("#selectlengthother").css("display","none");

			}
			if(a == "blusa")
			{
				$("#selectneckpp").css("display","none");
				$("#selectshoulderpp").css("display","block");
				$("#selectbustpp").css("display","block");
				$("#selecttrunkpp").css("display","block");
				$("#selectwaistpp").css("display","block");
				$("#selecthippp").css("display","none");
				$("#selectarmpp").css("display","block");
				$("#selectlegspp").css("display","none");
				$("#selecthandlepp").css("display","none");
				$("#selecthookpp").css("display","none");
				$("#selectthighpp").css("display","none");
				$("#selectcalfpp").css("display","none");
				$("#selectopeningBarpp").css("display","none");
				$("#selectbarWidthpp").css("display","none");
				$("#selectlengthpp").css("display","none");

				$("#selectneckp").css("display","none");
				$("#selectshoulderp").css("display","block");
				$("#selectbustp").css("display","block");
				$("#selecttrunkp").css("display","block");
				$("#selectwaistp").css("display","block");
				$("#selecthipp").css("display","none");
				$("#selectarmp").css("display","block");
				$("#selectlegsp").css("display","none");
				$("#selecthandlep").css("display","none");
				$("#selecthookp").css("display","none");
				$("#selectthighp").css("display","none");
				$("#selectcalfp").css("display","none");
				$("#selectopeningBarp").css("display","none");
				$("#selectbarWidthp").css("display","none");
				$("#selectlengthp").css("display","none");

				$("#selectneckm").css("display","none");
				$("#selectshoulderm").css("display","block");
				$("#selectbustm").css("display","block");
				$("#selecttrunkm").css("display","block");
				$("#selectwaistm").css("display","block");
				$("#selecthipm").css("display","none");
				$("#selectarmm").css("display","block");
				$("#selectlegsm").css("display","none");
				$("#selecthandlem").css("display","none");
				$("#selecthookm").css("display","none");
				$("#selectthighm").css("display","none");
				$("#selectcalfm").css("display","none");
				$("#selectopeningBarm").css("display","none");
				$("#selectbarWidthm").css("display","none");
				$("#selectlengthm").css("display","none");

				$("#selectneckg").css("display","none");
				$("#selectshoulderg").css("display","block");
				$("#selectbustg").css("display","block");
				$("#selecttrunkg").css("display","block");
				$("#selectwaistg").css("display","block");
				$("#selecthipg").css("display","none");
				$("#selectarmg").css("display","block");
				$("#selectlegsg").css("display","none");
				$("#selecthandleg").css("display","none");
				$("#selecthookg").css("display","none");
				$("#selectthighg").css("display","none");
				$("#selectcalfg").css("display","none");
				$("#selectopeningBarg").css("display","none");
				$("#selectbarWidthg").css("display","none");
				$("#selectlengthg").css("display","none");

				$("#selectneckgg").css("display","none");
				$("#selectshouldergg").css("display","block");
				$("#selectbustgg").css("display","block");
				$("#selecttrunkgg").css("display","block");
				$("#selectwaistgg").css("display","block");
				$("#selecthipgg").css("display","none");
				$("#selectarmgg").css("display","block");
				$("#selectlegsgg").css("display","none");
				$("#selecthandlegg").css("display","none");
				$("#selecthookgg").css("display","none");
				$("#selectthighgg").css("display","none");
				$("#selectcalfgg").css("display","none");
				$("#selectopeningBargg").css("display","none");
				$("#selectbarWidthgg").css("display","none");
				$("#selectlengthgg").css("display","none");

				$("#selectneckxg").css("display","none");
				$("#selectshoulderxg").css("display","block");
				$("#selectbustxg").css("display","block");
				$("#selecttrunkxg").css("display","block");
				$("#selectwaistxg").css("display","block");
				$("#selecthipxg").css("display","none");
				$("#selectarmxg").css("display","block");
				$("#selectlegsxg").css("display","none");
				$("#selecthandlexg").css("display","none");
				$("#selecthookxg").css("display","none");
				$("#selectthighxg").css("display","none");
				$("#selectcalfxg").css("display","none");
				$("#selectopeningBarxg").css("display","none");
				$("#selectbarWidthxg").css("display","none");
				$("#selectlengthxg").css("display","none");

				$("#selectneckxxg").css("display","none");
				$("#selectshoulderxxg").css("display","block");
				$("#selectbustxxg").css("display","block");
				$("#selecttrunkxxg").css("display","block");
				$("#selectwaistxxg").css("display","block");
				$("#selecthipxxg").css("display","none");
				$("#selectarmxxg").css("display","block");
				$("#selectlegsxxg").css("display","none");
				$("#selecthandlexxg").css("display","none");
				$("#selecthookxxg").css("display","none");
				$("#selectthighxxg").css("display","none");
				$("#selectcalfxxg").css("display","none");
				$("#selectopeningBarxxg").css("display","none");
				$("#selectbarWidthxxg").css("display","none");
				$("#selectlengthxxg").css("display","none");

				$("#selectneckother").css("display","none");
				$("#selectshoulderother").css("display","block");
				$("#selectbustother").css("display","block");
				$("#selecttrunkother").css("display","block");
				$("#selectwaistother").css("display","block");
				$("#selecthipother").css("display","none");
				$("#selectarmother").css("display","block");
				$("#selectlegsother").css("display","none");
				$("#selecthandleother").css("display","none");
				$("#selecthookother").css("display","none");
				$("#selectthighother").css("display","none");
				$("#selectcalfother").css("display","none");
				$("#selectopeningBarother").css("display","none");
				$("#selectbarWidthother").css("display","none");
				$("#selectlengthother").css("display","none");

			}
			if(a == "blazer")
			{
				$("#selectneckpp").css("display","none");
				$("#selectshoulderpp").css("display","block");
				$("#selectbustpp").css("display","block");
				$("#selecttrunkpp").css("display","block");
				$("#selectwaistpp").css("display","block");
				$("#selecthippp").css("display","none");
				$("#selectarmpp").css("display","block");
				$("#selectlegspp").css("display","none");
				$("#selecthandlepp").css("display","none");
				$("#selecthookpp").css("display","none");
				$("#selectthighpp").css("display","none");
				$("#selectcalfpp").css("display","none");
				$("#selectopeningBarpp").css("display","none");
				$("#selectbarWidthpp").css("display","none");
				$("#selectlengthpp").css("display","none");

				$("#selectneckp").css("display","none");
				$("#selectshoulderp").css("display","block");
				$("#selectbustp").css("display","block");
				$("#selecttrunkp").css("display","block");
				$("#selectwaistp").css("display","block");
				$("#selecthipp").css("display","none");
				$("#selectarmp").css("display","block");
				$("#selectlegsp").css("display","none");
				$("#selecthandlep").css("display","none");
				$("#selecthookp").css("display","none");
				$("#selectthighp").css("display","none");
				$("#selectcalfp").css("display","none");
				$("#selectopeningBarp").css("display","none");
				$("#selectbarWidthp").css("display","none");
				$("#selectlengthp").css("display","none");

				$("#selectneckm").css("display","none");
				$("#selectshoulderm").css("display","block");
				$("#selectbustm").css("display","block");
				$("#selecttrunkm").css("display","block");
				$("#selectwaistm").css("display","block");
				$("#selecthipm").css("display","none");
				$("#selectarmm").css("display","block");
				$("#selectlegsm").css("display","none");
				$("#selecthandlem").css("display","none");
				$("#selecthookm").css("display","none");
				$("#selectthighm").css("display","none");
				$("#selectcalfm").css("display","none");
				$("#selectopeningBarm").css("display","none");
				$("#selectbarWidthm").css("display","none");
				$("#selectlengthm").css("display","none");

				$("#selectneckg").css("display","none");
				$("#selectshoulderg").css("display","block");
				$("#selectbustg").css("display","block");
				$("#selecttrunkg").css("display","block");
				$("#selectwaistg").css("display","block");
				$("#selecthipg").css("display","none");
				$("#selectarmg").css("display","block");
				$("#selectlegsg").css("display","none");
				$("#selecthandleg").css("display","none");
				$("#selecthookg").css("display","none");
				$("#selectthighg").css("display","none");
				$("#selectcalfg").css("display","none");
				$("#selectopeningBarg").css("display","none");
				$("#selectbarWidthg").css("display","none");
				$("#selectlengthg").css("display","none");

				$("#selectneckxg").css("display","none");
				$("#selectshoulderxg").css("display","block");
				$("#selectbustxg").css("display","block");
				$("#selecttrunkxg").css("display","block");
				$("#selectwaistxg").css("display","block");
				$("#selecthipxg").css("display","none");
				$("#selectarmxg").css("display","block");
				$("#selectlegsxg").css("display","none");
				$("#selecthandlexg").css("display","none");
				$("#selecthookxg").css("display","none");
				$("#selectthighxg").css("display","none");
				$("#selectcalfxg").css("display","none");
				$("#selectopeningBarxg").css("display","none");
				$("#selectbarWidthxg").css("display","none");
				$("#selectlengthxg").css("display","none");

				$("#selectneckxxg").css("display","none");
				$("#selectshoulderxxg").css("display","block");
				$("#selectbustxxg").css("display","block");
				$("#selecttrunkxxg").css("display","block");
				$("#selectwaistxxg").css("display","block");
				$("#selecthipxxg").css("display","none");
				$("#selectarmxxg").css("display","block");
				$("#selectlegsxxg").css("display","none");
				$("#selecthandlexxg").css("display","none");
				$("#selecthookxxg").css("display","none");
				$("#selectthighxxg").css("display","none");
				$("#selectcalfxxg").css("display","none");
				$("#selectopeningBarxxg").css("display","none");
				$("#selectbarWidthxxg").css("display","none");
				$("#selectlengthxxg").css("display","none");

				$("#selectneckother").css("display","none");
				$("#selectshoulderother").css("display","block");
				$("#selectbustother").css("display","block");
				$("#selecttrunkother").css("display","block");
				$("#selectwaistother").css("display","block");
				$("#selecthipother").css("display","none");
				$("#selectarmother").css("display","block");
				$("#selectlegsother").css("display","none");
				$("#selecthandleother").css("display","none");
				$("#selecthookother").css("display","none");
				$("#selectthighother").css("display","none");
				$("#selectcalfother").css("display","none");
				$("#selectopeningBarother").css("display","none");
				$("#selectbarWidthother").css("display","none");
				$("#selectlengthother").css("display","none");

			}
			if(a == "casacos")
			{
				$("#selectneckpp").css("display","none");
				$("#selectshoulderpp").css("display","block");
				$("#selectbustpp").css("display","block");
				$("#selecttrunkpp").css("display","block");
				$("#selectwaistpp").css("display","block");
				$("#selecthippp").css("display","none");
				$("#selectarmpp").css("display","block");
				$("#selectlegspp").css("display","none");
				$("#selecthandlepp").css("display","none");
				$("#selecthookpp").css("display","none");
				$("#selectthighpp").css("display","none");
				$("#selectcalfpp").css("display","none");
				$("#selectopeningBarpp").css("display","none");
				$("#selectbarWidthpp").css("display","none");
				$("#selectlengthpp").css("display","none");

				$("#selectneckp").css("display","none");
				$("#selectshoulderp").css("display","block");
				$("#selectbustp").css("display","block");
				$("#selecttrunkp").css("display","block");
				$("#selectwaistp").css("display","block");
				$("#selecthipp").css("display","none");
				$("#selectarmp").css("display","block");
				$("#selectlegsp").css("display","none");
				$("#selecthandlep").css("display","none");
				$("#selecthookp").css("display","none");
				$("#selectthighp").css("display","none");
				$("#selectcalfp").css("display","none");
				$("#selectopeningBarp").css("display","none");
				$("#selectbarWidthp").css("display","none");
				$("#selectlengthp").css("display","none");

				$("#selectneckm").css("display","none");
				$("#selectshoulderm").css("display","block");
				$("#selectbustm").css("display","block");
				$("#selecttrunkm").css("display","block");
				$("#selectwaistm").css("display","block");
				$("#selecthipm").css("display","none");
				$("#selectarmm").css("display","block");
				$("#selectlegsm").css("display","none");
				$("#selecthandlem").css("display","none");
				$("#selecthookm").css("display","none");
				$("#selectthighm").css("display","none");
				$("#selectcalfm").css("display","none");
				$("#selectopeningBarm").css("display","none");
				$("#selectbarWidthm").css("display","none");
				$("#selectlengthm").css("display","none");

				$("#selectneckg").css("display","none");
				$("#selectshoulderg").css("display","block");
				$("#selectbustg").css("display","block");
				$("#selecttrunkg").css("display","block");
				$("#selectwaistg").css("display","block");
				$("#selecthipg").css("display","none");
				$("#selectarmg").css("display","block");
				$("#selectlegsg").css("display","none");
				$("#selecthandleg").css("display","none");
				$("#selecthookg").css("display","none");
				$("#selectthighg").css("display","none");
				$("#selectcalfg").css("display","none");
				$("#selectopeningBarg").css("display","none");
				$("#selectbarWidthg").css("display","none");
				$("#selectlengthg").css("display","none");

				$("#selectneckxg").css("display","none");
				$("#selectshoulderxg").css("display","block");
				$("#selectbustxg").css("display","block");
				$("#selecttrunkxg").css("display","block");
				$("#selectwaistxg").css("display","block");
				$("#selecthipxg").css("display","none");
				$("#selectarmxg").css("display","block");
				$("#selectlegsxg").css("display","none");
				$("#selecthandlexg").css("display","none");
				$("#selecthookxg").css("display","none");
				$("#selectthighxg").css("display","none");
				$("#selectcalfxg").css("display","none");
				$("#selectopeningBarxg").css("display","none");
				$("#selectbarWidthxg").css("display","none");
				$("#selectlengthxg").css("display","none");

				$("#selectneckxxg").css("display","none");
				$("#selectshoulderxxg").css("display","block");
				$("#selectbustxxg").css("display","block");
				$("#selecttrunkxxg").css("display","block");
				$("#selectwaistxxg").css("display","block");
				$("#selecthipxxg").css("display","none");
				$("#selectarmxxg").css("display","block");
				$("#selectlegsxxg").css("display","none");
				$("#selecthandlexxg").css("display","none");
				$("#selecthookxxg").css("display","none");
				$("#selectthighxxg").css("display","none");
				$("#selectcalfxxg").css("display","none");
				$("#selectopeningBarxxg").css("display","none");
				$("#selectbarWidthxxg").css("display","none");
				$("#selectlengthxxg").css("display","none");

				$("#selectneckother").css("display","none");
				$("#selectshoulderother").css("display","block");
				$("#selectbustother").css("display","block");
				$("#selecttrunkother").css("display","block");
				$("#selectwaistother").css("display","block");
				$("#selecthipother").css("display","none");
				$("#selectarmother").css("display","block");
				$("#selectlegsother").css("display","none");
				$("#selecthandleother").css("display","none");
				$("#selecthookother").css("display","none");
				$("#selectthighother").css("display","none");
				$("#selectcalfother").css("display","none");
				$("#selectopeningBarother").css("display","none");
				$("#selectbarWidthother").css("display","none");
				$("#selectlengthother").css("display","none");
			}


			if(a == "terno")
			{
				$("#selectneckpp").css("display","none");
				$("#selectshoulderpp").css("display","block");
				$("#selectbustpp").css("display","block");
				$("#selecttrunkpp").css("display","block");
				$("#selectwaistpp").css("display","block");
				$("#selecthippp").css("display","none");
				$("#selectarmpp").css("display","block");
				$("#selectlegspp").css("display","none");
				$("#selecthandlepp").css("display","none");
				$("#selecthookpp").css("display","none");
				$("#selectthighpp").css("display","none");
				$("#selectcalfpp").css("display","none");
				$("#selectopeningBarpp").css("display","none");
				$("#selectbarWidthpp").css("display","none");
				$("#selectlengthpp").css("display","none");

				$("#selectneckp").css("display","none");
				$("#selectshoulderp").css("display","block");
				$("#selectbustp").css("display","block");
				$("#selecttrunkp").css("display","block");
				$("#selectwaistp").css("display","block");
				$("#selecthipp").css("display","none");
				$("#selectarmp").css("display","block");
				$("#selectlegsp").css("display","none");
				$("#selecthandlep").css("display","none");
				$("#selecthookp").css("display","none");
				$("#selectthighp").css("display","none");
				$("#selectcalfp").css("display","none");
				$("#selectopeningBarp").css("display","none");
				$("#selectbarWidthp").css("display","none");
				$("#selectlengthp").css("display","none");

				$("#selectneckm").css("display","none");
				$("#selectshoulderm").css("display","block");
				$("#selectbustm").css("display","block");
				$("#selecttrunkm").css("display","block");
				$("#selectwaistm").css("display","block");
				$("#selecthipm").css("display","none");
				$("#selectarmm").css("display","block");
				$("#selectlegsm").css("display","none");
				$("#selecthandlem").css("display","none");
				$("#selecthookm").css("display","none");
				$("#selectthighm").css("display","none");
				$("#selectcalfm").css("display","none");
				$("#selectopeningBarm").css("display","none");
				$("#selectbarWidthm").css("display","none");
				$("#selectlengthm").css("display","none");

				$("#selectneckg").css("display","none");
				$("#selectshoulderg").css("display","block");
				$("#selectbustg").css("display","block");
				$("#selecttrunkg").css("display","block");
				$("#selectwaistg").css("display","block");
				$("#selecthipg").css("display","none");
				$("#selectarmg").css("display","block");
				$("#selectlegsg").css("display","none");
				$("#selecthandleg").css("display","none");
				$("#selecthookg").css("display","none");
				$("#selectthighg").css("display","none");
				$("#selectcalfg").css("display","none");
				$("#selectopeningBarg").css("display","none");
				$("#selectbarWidthg").css("display","none");
				$("#selectlengthg").css("display","none");

				$("#selectneckxg").css("display","none");
				$("#selectshoulderxg").css("display","block");
				$("#selectbustxg").css("display","block");
				$("#selecttrunkxg").css("display","block");
				$("#selectwaistxg").css("display","block");
				$("#selecthipxg").css("display","none");
				$("#selectarmxg").css("display","block");
				$("#selectlegsxg").css("display","none");
				$("#selecthandlexg").css("display","none");
				$("#selecthookxg").css("display","none");
				$("#selectthighxg").css("display","none");
				$("#selectcalfxg").css("display","none");
				$("#selectopeningBarxg").css("display","none");
				$("#selectbarWidthxg").css("display","none");
				$("#selectlengthxg").css("display","none");

				$("#selectneckxxg").css("display","none");
				$("#selectshoulderxxg").css("display","block");
				$("#selectbustxxg").css("display","block");
				$("#selecttrunkxxg").css("display","block");
				$("#selectwaistxxg").css("display","block");
				$("#selecthipxxg").css("display","none");
				$("#selectarmxxg").css("display","block");
				$("#selectlegsxxg").css("display","none");
				$("#selecthandlexxg").css("display","none");
				$("#selecthookxxg").css("display","none");
				$("#selectthighxxg").css("display","none");
				$("#selectcalfxxg").css("display","none");
				$("#selectopeningBarxxg").css("display","none");
				$("#selectbarWidthxxg").css("display","none");
				$("#selectlengthxxg").css("display","none");

				$("#selectneckother").css("display","none");
				$("#selectshoulderother").css("display","block");
				$("#selectbustother").css("display","block");
				$("#selecttrunkother").css("display","block");
				$("#selectwaistother").css("display","block");
				$("#selecthipother").css("display","none");
				$("#selectarmother").css("display","block");
				$("#selectlegsother").css("display","none");
				$("#selecthandleother").css("display","none");
				$("#selecthookother").css("display","none");
				$("#selectthighother").css("display","none");
				$("#selectcalfother").css("display","none");
				$("#selectopeningBarother").css("display","none");
				$("#selectbarWidthother").css("display","none");
				$("#selectlengthother").css("display","none");

			}
			if(a =="calca")
			{
				$("#selectneckpp").css("display","none");
				$("#selectshoulderpp").css("display","none");
				$("#selectbustpp").css("display","none");
				$("#selecttrunkpp").css("display","none");
				$("#selectwaistpp").css("display","block");
				$("#selecthippp").css("display","block");
				$("#selectarmpp").css("display","none");
				$("#selectlegspp").css("display","block");
				$("#selecthandlepp").css("display","none");
				$("#selecthookpp").css("display","block");
				$("#selectthighpp").css("display","block");
				$("#selectcalfpp").css("display","block");
				$("#selectopeningBarpp").css("display","none");
				$("#selectbarWidthpp").css("display","none");
				$("#selectlengthpp").css("display","none");

				$("#selectneckp").css("display","none");
				$("#selectshoulderp").css("display","none");
				$("#selectbustp").css("display","none");
				$("#selecttrunkp").css("display","none");
				$("#selectwaistp").css("display","block");
				$("#selecthipp").css("display","block");
				$("#selectarmp").css("display","none");
				$("#selectlegsp").css("display","block");
				$("#selecthandlep").css("display","none");
				$("#selecthookp").css("display","block");
				$("#selectthighp").css("display","block");
				$("#selectcalfp").css("display","block");
				$("#selectopeningBarp").css("display","none");
				$("#selectbarWidthp").css("display","none");
				$("#selectlengthp").css("display","none");

				$("#selectneckm").css("display","none");
				$("#selectshoulderm").css("display","none");
				$("#selectbustm").css("display","none");
				$("#selecttrunkm").css("display","none");
				$("#selectwaistm").css("display","block");
				$("#selecthipm").css("display","block");
				$("#selectarmm").css("display","none");
				$("#selectlegsm").css("display","block");
				$("#selecthandlem").css("display","none");
				$("#selecthookm").css("display","block");
				$("#selectthighm").css("display","block");
				$("#selectcalfm").css("display","block");
				$("#selectopeningBarm").css("display","none");
				$("#selectbarWidthm").css("display","none");
				$("#selectlengthm").css("display","none");

				$("#selectneckg").css("display","none");
				$("#selectshoulderg").css("display","none");
				$("#selectbustg").css("display","none");
				$("#selecttrunkg").css("display","none");
				$("#selectwaistg").css("display","block");
				$("#selecthipg").css("display","block");
				$("#selectarmg").css("display","none");
				$("#selectlegsg").css("display","block");
				$("#selecthandleg").css("display","none");
				$("#selecthookg").css("display","block");
				$("#selectthighg").css("display","block");
				$("#selectcalfg").css("display","block");
				$("#selectopeningBarg").css("display","none");
				$("#selectbarWidthg").css("display","none");
				$("#selectlengthg").css("display","none");

				$("#selectneckgg").css("display","none");
				$("#selectshouldergg").css("display","none");
				$("#selectbustgg").css("display","none");
				$("#selecttrunkgg").css("display","none");
				$("#selectwaistgg").css("display","block");
				$("#selecthipgg").css("display","block");
				$("#selectarmgg").css("display","none");
				$("#selectlegsgg").css("display","block");
				$("#selecthandlegg").css("display","none");
				$("#selecthookgg").css("display","block");
				$("#selectthighgg").css("display","block");
				$("#selectcalfgg").css("display","block");
				$("#selectopeningBargg").css("display","none");
				$("#selectbarWidthgg").css("display","none");
				$("#selectlengthgg").css("display","none");

				$("#selectneckxg").css("display","none");
				$("#selectshoulderxg").css("display","none");
				$("#selectbustxg").css("display","none");
				$("#selecttrunkxg").css("display","none");
				$("#selectwaistxg").css("display","block");
				$("#selecthipxg").css("display","block");
				$("#selectarmxg").css("display","none");
				$("#selectlegsxg").css("display","block");
				$("#selecthandlexg").css("display","none");
				$("#selecthookxg").css("display","block");
				$("#selectthighxg").css("display","block");
				$("#selectcalfxg").css("display","block");
				$("#selectopeningBarxg").css("display","none");
				$("#selectbarWidthxg").css("display","none");
				$("#selectlengthxg").css("display","none");

				$("#selectneckxxg").css("display","none");
				$("#selectshoulderxxg").css("display","none");
				$("#selectbustxxg").css("display","none");
				$("#selecttrunkxxg").css("display","none");
				$("#selectwaistxxg").css("display","block");
				$("#selecthipxxg").css("display","block");
				$("#selectarmxxg").css("display","none");
				$("#selectlegsxxg").css("display","block");
				$("#selecthandlexxg").css("display","none");
				$("#selecthookxxg").css("display","block");
				$("#selectthighxxg").css("display","block");
				$("#selectcalfxxg").css("display","block");
				$("#selectopeningBarxxg").css("display","none");
				$("#selectbarWidthxxg").css("display","none");
				$("#selectlengthxxg").css("display","none");

				$("#selectneckother").css("display","none");
				$("#selectshoulderother").css("display","none");
				$("#selectbustother").css("display","none");
				$("#selecttrunkother").css("display","none");
				$("#selectwaistother").css("display","block");
				$("#selecthipother").css("display","block");
				$("#selectarmother").css("display","none");
				$("#selectlegsother").css("display","block");
				$("#selecthandleother").css("display","none");
				$("#selecthookother").css("display","block");
				$("#selectthighother").css("display","block");
				$("#selectcalfother").css("display","block");
				$("#selectopeningBarother").css("display","none");
				$("#selectbarWidthother").css("display","none");
				$("#selectlengthother").css("display","none");
			}

			if(a =="bermuda" || a =="short")
			{
				$("#selectneckpp").css("display","none");
				$("#selectshoulderpp").css("display","none");
				$("#selectbustpp").css("display","none");
				$("#selecttrunkpp").css("display","none");
				$("#selectwaistpp").css("display","block");
				$("#selecthippp").css("display","block");
				$("#selectarmpp").css("display","none");
				$("#selectlegspp").css("display","block");
				$("#selecthandlepp").css("display","none");
				$("#selecthookpp").css("display","block");
				$("#selectthighpp").css("display","block");
				$("#selectcalfpp").css("display","none");
				$("#selectopeningBarpp").css("display","none");
				$("#selectbarWidthpp").css("display","none");
				$("#selectlengthpp").css("display","none");

				$("#selectneckp").css("display","none");
				$("#selectshoulderp").css("display","none");
				$("#selectbustp").css("display","none");
				$("#selecttrunkp").css("display","none");
				$("#selectwaistp").css("display","block");
				$("#selecthipp").css("display","block");
				$("#selectarmp").css("display","none");
				$("#selectlegsp").css("display","block");
				$("#selecthandlep").css("display","none");
				$("#selecthookp").css("display","block");
				$("#selectthighp").css("display","block");
				$("#selectcalfp").css("display","none");
				$("#selectopeningBarp").css("display","none");
				$("#selectbarWidthp").css("display","none");
				$("#selectlengthp").css("display","none");

				$("#selectneckm").css("display","none");
				$("#selectshoulderm").css("display","none");
				$("#selectbustm").css("display","none");
				$("#selecttrunkm").css("display","none");
				$("#selectwaistm").css("display","block");
				$("#selecthipm").css("display","block");
				$("#selectarmm").css("display","none");
				$("#selectlegsm").css("display","block");
				$("#selecthandlem").css("display","none");
				$("#selecthookm").css("display","block");
				$("#selectthighm").css("display","block");
				$("#selectcalfm").css("display","none");
				$("#selectopeningBarm").css("display","none");
				$("#selectbarWidthm").css("display","none");
				$("#selectlengthm").css("display","none");

				$("#selectneckg").css("display","none");
				$("#selectshoulderg").css("display","none");
				$("#selectbustg").css("display","none");
				$("#selecttrunkg").css("display","none");
				$("#selectwaistg").css("display","block");
				$("#selecthipg").css("display","block");
				$("#selectarmg").css("display","none");
				$("#selectlegsg").css("display","block");
				$("#selecthandleg").css("display","none");
				$("#selecthookg").css("display","block");
				$("#selectthighg").css("display","block");
				$("#selectcalfg").css("display","none");
				$("#selectopeningBarg").css("display","none");
				$("#selectbarWidthg").css("display","none");
				$("#selectlengthg").css("display","none");

				$("#selectneckgg").css("display","none");
				$("#selectshouldergg").css("display","none");
				$("#selectbustgg").css("display","none");
				$("#selecttrunkgg").css("display","none");
				$("#selectwaistgg").css("display","block");
				$("#selecthipgg").css("display","block");
				$("#selectarmgg").css("display","none");
				$("#selectlegsgg").css("display","block");
				$("#selecthandlegg").css("display","none");
				$("#selecthookgg").css("display","block");
				$("#selectthighgg").css("display","block");
				$("#selectcalfgg").css("display","none");
				$("#selectopeningBargg").css("display","none");
				$("#selectbarWidthgg").css("display","none");
				$("#selectlengthgg").css("display","none");

				$("#selectneckxg").css("display","none");
				$("#selectshoulderxg").css("display","none");
				$("#selectbustxg").css("display","none");
				$("#selecttrunkxg").css("display","none");
				$("#selectwaistxg").css("display","block");
				$("#selecthipxg").css("display","block");
				$("#selectarmxg").css("display","none");
				$("#selectlegsxg").css("display","block");
				$("#selecthandlexg").css("display","none");
				$("#selecthookxg").css("display","block");
				$("#selectthighxg").css("display","block");
				$("#selectcalfxg").css("display","none");
				$("#selectopeningBarxg").css("display","none");
				$("#selectbarWidthxg").css("display","none");
				$("#selectlengthxg").css("display","none");

				$("#selectneckxxg").css("display","none");
				$("#selectshoulderxxg").css("display","none");
				$("#selectbustxxg").css("display","none");
				$("#selecttrunkxxg").css("display","none");
				$("#selectwaistxxg").css("display","block");
				$("#selecthipxxg").css("display","block");
				$("#selectarmxxg").css("display","none");
				$("#selectlegsxxg").css("display","block");
				$("#selecthandlexxg").css("display","none");
				$("#selecthookxxg").css("display","block");
				$("#selectthighxxg").css("display","block");
				$("#selectcalfxxg").css("display","none");
				$("#selectopeningBarxxg").css("display","none");
				$("#selectbarWidthxxg").css("display","none");
				$("#selectlengthxxg").css("display","none");

				$("#selectneckother").css("display","none");
				$("#selectshoulderother").css("display","none");
				$("#selectbustother").css("display","none");
				$("#selecttrunkother").css("display","none");
				$("#selectwaistother").css("display","block");
				$("#selecthipother").css("display","block");
				$("#selectarmother").css("display","none");
				$("#selectlegsother").css("display","block");
				$("#selecthandleother").css("display","none");
				$("#selecthookother").css("display","block");
				$("#selectthighother").css("display","block");
				$("#selectcalfother").css("display","none");
				$("#selectopeningBarother").css("display","none");
				$("#selectbarWidthother").css("display","none");
				$("#selectlengthother").css("display","none");
			}

			if(a == "saia")
			{
				$("#selectneckpp").css("display","none");
				$("#selectshoulderpp").css("display","none");
				$("#selectbustpp").css("display","none");
				$("#selecttrunkpp").css("display","none");
				$("#selectwaistpp").css("display","block");
				$("#selecthippp").css("display","block");
				$("#selectarmpp").css("display","none");
				$("#selectlegspp").css("display","none");
				$("#selecthandlepp").css("display","none");
				$("#selecthookpp").css("display","none");
				$("#selectthighpp").css("display","none");
				$("#selectcalfpp").css("display","none");
				$("#selectopeningBarpp").css("display","block");
				$("#selectbarWidthpp").css("display","none");
				$("#selectlengthpp").css("display","block");

				$("#selectneckp").css("display","none");
				$("#selectshoulderp").css("display","none");
				$("#selectbustp").css("display","none");
				$("#selecttrunkp").css("display","none");
				$("#selectwaistp").css("display","block");
				$("#selecthipp").css("display","block");
				$("#selectarmp").css("display","none");
				$("#selectlegsp").css("display","none");
				$("#selecthandlep").css("display","none");
				$("#selecthookp").css("display","none");
				$("#selectthighp").css("display","none");
				$("#selectcalfp").css("display","none");
				$("#selectopeningBarp").css("display","block");
				$("#selectbarWidthp").css("display","none");
				$("#selectlengthp").css("display","block");

				$("#selectneckm").css("display","none");
				$("#selectshoulderm").css("display","none");
				$("#selectbustm").css("display","none");
				$("#selecttrunkm").css("display","none");
				$("#selectwaistm").css("display","block");
				$("#selecthipm").css("display","block");
				$("#selectarmm").css("display","none");
				$("#selectlegsm").css("display","none");
				$("#selecthandlem").css("display","none");
				$("#selecthookm").css("display","none");
				$("#selectthighm").css("display","none");
				$("#selectcalfm").css("display","none");
				$("#selectopeningBarm").css("display","block");
				$("#selectbarWidthm").css("display","none");
				$("#selectlengthm").css("display","block");

				$("#selectneckg").css("display","none");
				$("#selectshoulderg").css("display","none");
				$("#selectbustg").css("display","none");
				$("#selecttrunkg").css("display","none");
				$("#selectwaistg").css("display","block");
				$("#selecthipg").css("display","block");
				$("#selectarmg").css("display","none");
				$("#selectlegsg").css("display","none");
				$("#selecthandleg").css("display","none");
				$("#selecthookg").css("display","none");
				$("#selectthighg").css("display","none");
				$("#selectcalfg").css("display","none");
				$("#selectopeningBarg").css("display","block");
				$("#selectbarWidthg").css("display","none");
				$("#selectlengthg").css("display","block");

				$("#selectneckgg").css("display","none");
				$("#selectshouldergg").css("display","none");
				$("#selectbustgg").css("display","none");
				$("#selecttrunkgg").css("display","none");
				$("#selectwaistgg").css("display","block");
				$("#selecthipgg").css("display","block");
				$("#selectarmgg").css("display","none");
				$("#selectlegsgg").css("display","none");
				$("#selecthandlegg").css("display","none");
				$("#selecthookgg").css("display","none");
				$("#selectthighgg").css("display","none");
				$("#selectcalfgg").css("display","none");
				$("#selectopeningBargg").css("display","block");
				$("#selectbarWidthgg").css("display","none");
				$("#selectlengthgg").css("display","block");

				$("#selectneckxg").css("display","none");
				$("#selectshoulderxg").css("display","none");
				$("#selectbustxg").css("display","none");
				$("#selecttrunkxg").css("display","none");
				$("#selectwaistxg").css("display","block");
				$("#selecthipxg").css("display","block");
				$("#selectarmxg").css("display","none");
				$("#selectlegsxg").css("display","none");
				$("#selecthandlexg").css("display","none");
				$("#selecthookxg").css("display","none");
				$("#selectthighxg").css("display","none");
				$("#selectcalfxg").css("display","none");
				$("#selectopeningBarxg").css("display","block");
				$("#selectbarWidthxg").css("display","none");
				$("#selectlengthxg").css("display","block");

				$("#selectneckxxg").css("display","none");
				$("#selectshoulderxxg").css("display","none");
				$("#selectbustxxg").css("display","none");
				$("#selecttrunkxxg").css("display","none");
				$("#selectwaistxxg").css("display","block");
				$("#selecthipxxg").css("display","block");
				$("#selectarmxxg").css("display","none");
				$("#selectlegsxxg").css("display","none");
				$("#selecthandlexxg").css("display","none");
				$("#selecthookxxg").css("display","none");
				$("#selectthighxxg").css("display","none");
				$("#selectcalfxxg").css("display","none");
				$("#selectopeningBarxxg").css("display","block");
				$("#selectbarWidthxxg").css("display","none");
				$("#selectlengthxxg").css("display","block");

				$("#selectneckother").css("display","none");
				$("#selectshoulderother").css("display","none");
				$("#selectbustother").css("display","none");
				$("#selecttrunkother").css("display","none");
				$("#selectwaistother").css("display","block");
				$("#selecthipother").css("display","block");
				$("#selectarmother").css("display","none");
				$("#selectlegsother").css("display","none");
				$("#selecthandleother").css("display","none");
				$("#selecthookother").css("display","none");
				$("#selectthighother").css("display","none");
				$("#selectcalfother").css("display","none");
				$("#selectopeningBarother").css("display","block");
				$("#selectbarWidthother").css("display","none");
				$("#selectlengthother").css("display","block");

			}
        });


/*    $("#camisa").click(function(evento){
		if ($("#camisa").attr("checked")){
			$("#selectneck").css("display", "block");
			$("#selectshoulder").css("display", "block");
			$("#selectbust").css("display", "block");
			$("#selectwaist").css("display", "block");
			$("#selectarm").css("display", "block");
			$("#selecthandle").css("display", "block");
			$("#selecttrunk").css("display", "block");
		}else{
			$("#selectneck").css("display", "none");
			$("#selectshoulder").css("display", "none");
			$("#selectbust").css("display", "none");
			$("#selectwaist").css("display", "none");
			$("#selectarm").css("display", "none");
			$("#selecthandle").css("display", "none");
			$("#selecttrunk").css("display", "none");
		}
   	});
    $("#camiseta").click(function(evento){
		if ($("#camiseta").attr("checked")){
			$("#selectshoulder").css("display", "block");
			$("#selectbust").css("display", "block");
			$("#selectwaist").css("display", "block");
			$("#selectarm").css("display", "block");
			$("#selecttrunk").css("display", "block");
		}else{
			$("#selectshoulder").css("display", "none");
			$("#selectbust").css("display", "none");
			$("#selectwaist").css("display", "none");
			$("#selectarm").css("display", "none");
			$("#selecttrunk").css("display", "none");
		}
   	});
    $("#blusa").click(function(evento){
		if ($("#blusa").attr("checked")){
			$("#selectshoulder").css("display", "block");
			$("#selectarm").css("display", "block");
			$("#selectbust").css("display", "block");
			$("#selectwaist").css("display", "block");
			$("#selecttrunk").css("display", "block");
		}else{
			$("#selectshoulder").css("display", "none");
			$("#selectarm").css("display", "none");
			$("#selectbust").css("display", "none");
			$("#selectwaist").css("display", "none");
			$("#selecttrunk").css("display", "none");
		}
   	});
    $("#blazer").click(function(evento){
		if ($("#blazer").attr("checked")){
			$("#selectshoulder").css("display", "block");
			$("#selectchest").css("display", "block");
			$("#selectarm").css("display", "block");
			$("#selectwaist").css("display", "block");
			$("#selectbust").css("display", "block");
			$("#selecttrunk").css("display", "block");
		}else{
			$("#selectshoulder").css("display", "none");
			$("#selectchest").css("display", "none");
			$("#selectarm").css("display", "none");
			$("#selectwaist").css("display", "none");
			$("#selectbust").css("display", "none");
			$("#selecttrunk").css("display", "none");
		}
   	});
    $("#casacos").click(function(evento){
		if ($("#casacos").attr("checked")){
			$("#selectshoulder").css("display", "block");
			$("#selectchest").css("display", "block");
			$("#selectarm").css("display", "block");
			$("#selectwaist").css("display", "block");
			$("#selectbust").css("display", "block");
			$("#selecttrunk").css("display", "block");
		}else{
			$("#selectshoulder").css("display", "none");
			$("#selectchest").css("display", "none");
			$("#selectarm").css("display", "none");
			$("#selectwaist").css("display", "none");
			$("#selectbust").css("display", "none");
			$("#selecttrunk").css("display", "none");
		}
   	});
    $("#moletom").click(function(evento){
		if ($("#moletom").attr("checked")){
			$("#selectshoulder").css("display", "block");
			$("#selectchest").css("display", "block");
			$("#selectarm").css("display", "block");
			$("#selectwaist").css("display", "block");
			$("#selectbust").css("display", "block");
			$("#selecttrunk").css("display", "block");
		}else{
			$("#selectshoulder").css("display", "none");
			$("#selectchest").css("display", "none");
			$("#selectarm").css("display", "none");
			$("#selectwaist").css("display", "none");
			$("#selectbust").css("display", "none");
			$("#selecttrunk").css("display", "none");
		}
   	});
    $("#jaqueta").click(function(evento){
		if ($("#jaqueta").attr("checked")){
			$("#selectshoulder").css("display", "block");
			$("#selectchest").css("display", "block");
			$("#selectarm").css("display", "block");
			$("#selectwaist").css("display", "block");
			$("#selectbust").css("display", "block");
			$("#selecttrunk").css("display", "block");
		}else{
			$("#selectshoulder").css("display", "none");
			$("#selectchest").css("display", "none");
			$("#selectarm").css("display", "none");
			$("#selectwaist").css("display", "none");
			$("#selectbust").css("display", "none");
			$("#selecttrunk").css("display", "none");
		}
   	});
    $("#calca").click(function(evento){
		if ($("#calca").attr("checked")){
			$("#selectwaist").css("display", "block");
			$("#selecthip").css("display", "block");
			$("#selecthook").css("display", "block");
			$("#selectlegs").css("display", "block");
			$("#selectthigh").css("display", "block");
			$("#selectcalf").css("display", "block");
		}else{
			$("#selectwaist").css("display", "none");
			$("#selecthip").css("display", "none");
			$("#selecthook").css("display", "none");
			$("#selectlegs").css("display", "none");
			$("#selectthigh").css("display", "none");
			$("#selectcalf").css("display", "none");
		}
   	});
    $("#bermuda").click(function(evento){
		if ($("#bermuda").attr("checked")){
			$("#selectwaist").css("display", "block");
			$("#selecthip").css("display", "block");
			$("#selecthook").css("display", "block");
			$("#selectlegs").css("display", "block");
			$("#selectthigh").css("display", "block");
		}else{
			$("#selectwaist").css("display", "none");
			$("#selecthip").css("display", "none");
			$("#selecthook").css("display", "none");
			$("#selectlegs").css("display", "none");
			$("#selectthigh").css("display", "none");
		}
   	});
    $("#short").click(function(evento){
		if ($("#short").attr("checked")){
			$("#selectwaist").css("display", "block");
			$("#selecthip").css("display", "block");
			$("#selecthook").css("display", "block");
			$("#selectlegs").css("display", "block");
			$("#selectthigh").css("display", "block");
		}else{
			$("#selectwaist").css("display", "none");
			$("#selecthip").css("display", "none");
			$("#selecthook").css("display", "none");
			$("#selectlegs").css("display", "none");
			$("#selectthigh").css("display", "none");
		}
   	});
    $("#saia").click(function(evento){
		if ($("#saia").attr("checked")){
			$("#selectwaist").css("display", "block");
			$("#selecthip").css("display", "block");
			$("#selectlength").css("display", "block");
			$("#selectopeningBar").css("display", "block");
		}else{
			$("#selectwaist").css("display", "none");
			$("#selecthip").css("display", "none");
			$("#selectlength").css("display", "none");
			$("#selectopeningBar").css("display", "none");
		}
   	});*/
});